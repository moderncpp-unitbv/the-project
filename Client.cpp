#include "Client.h"
#include "LobbyScreen.h"
#include "PsychicScreen.h"
#include "GhostScreen.h"
#include "JoiningScreen.h"

Client::Client()
{
	socket = new sf::TcpSocket();
	
	connected = false;
}

void Client::StartConnectThread(sf::IpAddress ipAddress)
{
	clientConnectThread = new sf::Thread(std::bind(&Client::Connect, this, ipAddress), this);
	clientConnectThread->launch();
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"

void Client::Connect(sf::IpAddress targetIpAddress)
{
	std::cout << "[CLIENT] Started Connecting..." << std::endl;
	connected = false;
	
	if (socket->connect(targetIpAddress, Game::CONNECTION_PORT, sf::seconds(2)) != sf::Socket::Status::Done)
	{
		std::cout << "[ERROR][CLIENT] Could not connect to IP Address: " << targetIpAddress << std::endl;
		return;
	}
	
	connected = true;
	std::cout << "[CLIENT] Connected." << std::endl;
	SendConnectedCommand();
	
	while (true)
	{
		sf::Packet receivedPacket;
		sf::Socket::Status status = socket->receive(receivedPacket);
		if (status == sf::Socket::Done)
		{
			this->ReadPacket(receivedPacket);
		}
		else if (status == sf::Socket::Disconnected)
		{
			Game::instance->getGraphicHandler()->setTargetScreen(new MainMenuScreen());
			std::cout << "[CLIENT] Disconnected." << std::endl;
			delete this;
		}
	}
}

#pragma clang diagnostic pop

void Client::ReadPacket(sf::Packet packet)
{
	std::cout << "[CLIENT] Received packet." << std::endl;
	sf::Int8 commandTypeInt;
	packet >> commandTypeInt;
	auto commandType = (CommandType) commandTypeInt;
	
	switch (commandType)
	{
		case CommandType::Connected:
		{
			auto *player = new Player();
			packet >> *player;
			Game::instance->AddPlayer(player);
			Game::instance->RedrawPlayerList();
			std::cout << "[CLIENT] Received connected command: " << player->GetName() << ", Role: "
			          << player->GetRoleName()
			          << std::endl;
			
			break;
		}
		case CommandType::Disconnected:
		{
			std::string name;
			packet >> name;
			Game::instance->RemovePlayerByName(name);
			Game::instance->RedrawPlayerList();
			std::cout << "[CLIENT] Received disconnected command: " << name << std::endl;
			
			break;
		}
		case CommandType::StartGame:
		{
			std::cout << "[CLIENT] Received command: START GAME." << std::endl;
			
			if (Game::instance->GetPlayer()->GetRole() == PlayerRole::Psychic_Role)
			{
				uint8_t psychicIndex;
				uint8_t characterCardIndex;
				uint8_t locationCardIndex;
				uint8_t objectCardIndex;
				
				packet >> psychicIndex;
				packet >> characterCardIndex;
				packet >> locationCardIndex;
				packet >> objectCardIndex;
				
				auto clue = new CardSet(Game::instance->GetCharacterDeck().at(characterCardIndex),
				                        Game::instance->GetLocationDeck().at(locationCardIndex),
				                        Game::instance->GetObjectDeck().at(objectCardIndex));
				
				Game::instance->GetPlayer()->InitializePsychic(clue, psychicIndex);
				Game::instance->getGraphicHandler()->setTargetScreen(new PsychicScreen());
			}
			else if (Game::instance->GetPlayer()->GetRole() == PlayerRole::Ghost_Role)
			{
				sf::Uint8 psychicIndex;
				sf::Uint8 characterCluesIndex;
				sf::Uint8 locationCluesIndex;
				sf::Uint8 objectCluesIndex;
				
				packet >> psychicIndex;
				packet >> characterCluesIndex;
				packet >> locationCluesIndex;
				packet >> objectCluesIndex;
				
				std::cout << "[CLIENT] Creating psychic index " << (int) psychicIndex << " with clues: "
				          << (int) characterCluesIndex << ", " << (int) locationCluesIndex << ", "
				          << (int) objectCluesIndex << std::endl;
				
				auto clue = new CardSet(Game::instance->GetCharacterDeck().at(characterCluesIndex),
				                        Game::instance->GetLocationDeck().at(locationCluesIndex),
				                        Game::instance->GetObjectDeck().at(objectCluesIndex));
				
				if (Game::instance->GetPlayer()->getGhost() == nullptr)
				{
					Game::instance->GetPlayer()->InitializeGhost();
				}
				
				Game::instance->GetPlayer()->getGhost()->CreatePsychic(clue, psychicIndex);
			}
			else
			{
				std::cout << "[CLIENT] [ERROR] Game started with a player not having a role." << std::endl;
			}
			
			break;
		}
		case CommandType::GhostStart:
		{
			if (Game::instance->GetPlayer()->getGhost() == nullptr)
				return;
			
			Game::instance->getGraphicHandler()->setTargetScreen(new GhostScreen());
			break;
		}
		case CommandType::PlayerList:
		{
			std::cout << "[CLIENT] Received player list command." << std::endl;
			sf::Uint8 readCountValue;
			packet >> readCountValue;
			
			auto count = (uint8_t) readCountValue;
			
			Game::instance->ClearPlayersList();
			for (int i = 0; i < count; ++i)
			{
				auto *newPlayer = new Player();
				packet >> *newPlayer;
				std::cout << "[CLIENT] Player: " << newPlayer->GetName() << " - " << newPlayer->GetRoleName()
				          << std::endl;
				Game::instance->AddPlayer(newPlayer);
			}
			
			Game::instance->getGraphicHandler()->setTargetScreen(new LobbyScreen());
			break;
		}
		case CommandType::Rejected:
		{
			JoiningScreen::setShowRejectedMessage(true);
			
			std::function<void()> HideRejectedMessageFunction = []()
			{
				JoiningScreen::setShowRejectedMessage(false);
			};
			
			Game::instance->getTimeHandler()->setTimer(&HideRejectedMessageFunction, 2);
			break;
		}
		case CommandType::RoleChange:
		{
			std::string playerName;
			packet >> playerName;
			
			sf::Int8 playerRole;
			packet >> playerRole;
			
			for (auto player: Game::instance->getPlayersList())
			{
				if (player->GetName() == playerName)
				{
					player->SetRole(static_cast<PlayerRole>(playerRole));
					std::cout << "[CLIENT] Player " << playerName << " changed his role to " << player->GetRoleName()
					          << std::endl;
					break;
				}
			}
			LobbyScreen::redrawPlayers();
			break;
		}
		case CommandType::VisionCards:
		{
			uint8_t size;
			packet >> size;
			
			std::set<uint8_t> cardsIDs;
			for (unsigned int index = 0; index < size; index++)
			{
				uint8_t ID;
				packet >> ID;
				std::cout << "[CLIENT] Read ID: " << (int) ID << std::endl;
				cardsIDs.insert(ID);
			}
			
			std::vector<VisionCard *> visions;
			for (auto cardID: cardsIDs)
			{
				auto visionCard = new VisionCard(cardID);
				visions.push_back(visionCard);
			}
			
			std::cout << "[CLIENT] Read visions size from packet: " << (int) size << '\n';
			std::cout << "[CLIENT] Received " << (int) visions.size() << " visions.\n";
			Game::instance->GetPlayer()->getPsychic()->SetVisions(visions);
			break;
		}
		case CommandType::Advance:
		{
			Clock::Advance();
			break;
		}
		case CommandType::GhostGuessInfo:
		{
			sf::Uint8 psychicID;
			packet >> psychicID;

			sf::Uint8 guessed;
			packet >> guessed;

			if ((int) guessed == 0)
			{
				GhostScreen::setTextFieldGuessingText(((int) psychicID) - 1, "correct");
			}
			else
			{
				GhostScreen::setTextFieldGuessingText(((int) psychicID) - 1, "incorrect");
			}
			break;
		}
		default:
		{
			std::cout << "[CLIENT] Invalid command type." << std::endl;
		}
	}
}

void Client::SendConnectedCommand()
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::Connected;
	
	packet << *Game::instance->GetPlayer();
	socket->send(packet);
	
	std::cout << "[CLIENT] Sent connected command: " << Game::instance->GetPlayer()->GetName()
	          << ", Role: " << Game::instance->GetPlayer()->GetRoleName() << std::endl;
}

void Client::SendDisconnectedCommand()
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::Disconnected;
	
	packet << Game::instance->GetPlayer()->GetName();
	socket->send(packet);
	
	std::cout << "[CLIENT] Sent disconnected command: " << Game::instance->GetPlayer()->GetName() << std::endl;
}

void Client::SendRoleChangeCommand()
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::RoleChange;
	
	packet << Game::instance->GetPlayer()->GetName();
	packet << (sf::Int8) Game::instance->GetPlayer()->GetRole();
	
	socket->send(packet);
}

void Client::SendAdvanceCommand(uint8_t guessed)
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::Advance;
	packet << (sf::Uint8) guessed;
	
	socket->send(packet);
}

bool Client::IsConnected() const
{
	return connected;
}

void Client::Disconnect()
{
	std::cout << "[CLIENT] Disconnecting..." << std::endl;
	
	clientConnectThread->terminate();
	delete clientConnectThread;
	
	SendDisconnectedCommand();
	socket->disconnect();
	
	connected = false;
}

Client::~Client()
{
	std::cout << "[CLIENT] Deleting client." << std::endl;
	clientConnectThread->terminate();
	delete clientConnectThread;
}

void Client::SendVisionCardsCommand(uint8_t psychicID, const std::vector<VisionCard *> &cards)
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::VisionCards;
	
	packet << (sf::Uint8) psychicID;
	packet << (sf::Uint8) cards.size();
	for (auto card: cards)
	{
		packet << (sf::Uint8) card->GetNumber();
	}
	
	std::cout << "[CLIENT] Sent vision cards command: psychic " << (int) psychicID << ", " << (int) cards.size()
	          << " visions.\n";
	socket->send(packet);
}

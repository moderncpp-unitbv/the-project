
#ifndef MYSTERIUM_TEXTFIELD_H
#define MYSTERIUM_TEXTFIELD_H


#include <functional>
#include <thread>
#include "SFML/Graphics.hpp"

class TextField
{
private:
	sf::RectangleShape *m_shape;
	sf::Texture *m_texture;
	sf::Text *m_text;
	sf::Font m_font;
	int m_characterSize;
	sf::Color m_color;
	
public:
	TextField(float x, float y, float width, float height, sf::Font *font, int characterSize, const std::string &text,
	       sf::Color color);
	~TextField() = default;

public:
	void renderTextField(sf::RenderWindow *window);
	
	void EditText(const std::string &text);
	
	void SetText(const std::string&, const unsigned int&, const unsigned int&);
	
	void SetColor(const sf::Color&);
	
	unsigned int  GetWidth();
	
	unsigned int  GetHeight();

	unsigned int GetTextSize();

	sf::Color GetTextColor();

	float GetXPosition();

	float GetYPosition();

	void CenterText();

};


#endif //MYSTERIUM_TEXTFIELD_H

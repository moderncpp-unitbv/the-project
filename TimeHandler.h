#ifndef MYSTERIUMGAME_TIMEHANDLER_H
#define MYSTERIUMGAME_TIMEHANDLER_H

#include <SFML/Graphics.hpp>
#include <functional>

class TimeHandler
{
public:
	TimeHandler();

private:
	std::map<std::function<void()> *, float> timers;
	sf::Clock startGameClock;
	sf::Clock lastFrameClock;

public:
	
	void setTimer(std::function<void()> *pFunction, float seconds);
	
	void removeTimer(std::function<void()> *pFunction);
	
	void updateTimers(float seconds);
	
	sf::Time getTimeFromStartGame();
	
	sf::Time getTimeFromLastFrame();
	
	sf::Time resetLastFrameClock();
};


#endif //MYSTERIUMGAME_TIMEHANDLER_H

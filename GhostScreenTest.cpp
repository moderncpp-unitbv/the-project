#include <gtest/gtest.h>
#include "GhostScreen.h"

class GhostScreenTest : public ::testing::Test
{
protected:
    void SetUp() override
    {}

    void TearDown() override
    {}
};

TEST_F(GhostScreenTest, IsCreated)
{
    auto *newScreen = new GhostScreen();

    EXPECT_LT(0, newScreen->GetVisionCardsSize());

//    EXPECT_LT(0, newScreen->GetCardSetsSize());
	
	EXPECT_LT(0, newScreen->GetWidth());
	
	EXPECT_LT(0, newScreen->GetHeight());
    
    delete newScreen;
}
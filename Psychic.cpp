#include "Psychic.h"
#include <utility>
#include "Game.h"

Psychic::Psychic(std::string name)
		: m_name(std::move(name))
{
}

Psychic::Psychic()
{
}

Psychic::Psychic(CardSet *cluesSet, const uint8_t& index)
		: m_cluesSet(cluesSet), m_indexInGame(index)
{
	auto sprite = new sf::Sprite();
	auto texture = new sf::Texture();
	texture->loadFromFile("Resources/Textures/tokens/ghost/gt" + std::to_string(index) + ".png");
	sprite->setTexture(*texture);
	sprite->setPosition(515, 100);
	sprite->setScale(0.5f, 0.5f);
	m_ghostToken = new sf::Sprite();
	m_ghostToken = sprite;
}

void Psychic::SetVisions(std::vector<VisionCard *> visions)
{
    m_visions = std::move(visions);
}

void Psychic::ReceiveVisions(std::vector<VisionCard *> visions)
{
	for (VisionCard *newCard: visions)
	{
		m_visions.push_back(newCard);
	}
}

void Psychic::SetCardSet(CardSet *cluesSet)
{
	m_cluesSet = cluesSet;
}

CardSet *Psychic::GetCardSet() const
{
	return m_cluesSet;
}

sf::Sprite *Psychic::GetGhostToken() const
{
	return m_ghostToken;
}

bool Psychic::HasReceivedVisions() const
{
	return (!m_visions.empty());
}

uint8_t Psychic::getPoints() const
{
    return m_points;
}

void Psychic::IncreasePoints()
{
    m_points++;
}

uint8_t Psychic::GetIndex() const
{
    return m_indexInGame;
}

std::vector<VisionCard *> Psychic::GetVisions() const
{
    return m_visions;
}

void Psychic::ResetVisions() {
    m_visions.clear();
}

bool Psychic::isGuessed() const
{
	return guessed;
}

void Psychic::setGuessed(bool guessed)
{
	Psychic::guessed = guessed;
}

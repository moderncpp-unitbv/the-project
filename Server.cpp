#include <list>
#include "Server.h"

Server::Server()
{
	connections = std::set<ClientConnection *>();
	
	listener = new sf::TcpListener();
	
	socketSelector = new sf::SocketSelector();
}

Server::~Server()
{
	listener->close();
	
	for (auto connection: connections)
	{
		connection->getSocket()->disconnect();
		delete connection;
	}
	
	serverListenThread->terminate();
	delete serverListenThread;
}

void Server::StartServer()
{
	serverListenThread = new sf::Thread(&Server::Listen, this);
	serverListenThread->launch();
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"

void Server::Listen()
{
	if (listener->listen(Game::CONNECTION_PORT) != sf::Socket::Done)
	{
		std::cout << "[SERVER] Error listening port: " << Game::CONNECTION_PORT << ". It may be already in use."
		          << std::endl;
		
		return;
	}
	
	socketSelector->add(*listener);
	
	while (true)
	{
		if (socketSelector->wait())
		{
			if (socketSelector->isReady(*listener))
			{
				auto *client = new sf::TcpSocket;
				auto status = listener->accept(*client);
				
				if (status == sf::Socket::Done)
				{
					std::cout << "[SERVER] Accepted client." << std::endl;
					
					auto connection = new ClientConnection(client);
					connections.insert(connection);
					socketSelector->add(*client);
				}
				else
				{
					delete client;
					
					if (status == sf::Socket::Disconnected)
					{
						std::cout << "[SERVER] Rejected client: Disconnected." << std::endl;
					}
					else if (status == sf::Socket::Partial)
					{
						std::cout << "[SERVER] Rejected client: Partial." << std::endl;
					}
					else if (status == sf::Socket::NotReady)
					{
						std::cout << "[SERVER] Rejected client: NotReady." << std::endl;
					}
					else if (status == sf::Socket::Error)
					{
						std::cout << "[SERVER] Rejected client: Error." << std::endl;
					}
					else
					{
						std::cout << "[SERVER] Rejected client: Unknown." << std::endl;
					}
				}
			}
			else
			{
				std::set<ClientConnection *> aux = std::set<ClientConnection *>(connections);
				for (auto it : aux)
				{
					sf::TcpSocket &client = *it->getSocket();
					sf::Packet packet;
					if (socketSelector->isReady(client))
					{
						sf::Socket::Status status = client.receive(packet);
						if (status == sf::Socket::Done)
						{
							std::cout << "[SERVER] Received a packet from client." << std::endl;
							this->ProcessPacket(client, packet);
						}
						else if (status == sf::Socket::Disconnected)
						{
							std::cout << "[SERVER] Disconnecting client..." << std::endl;
							socketSelector->remove(client);
							connections.erase(it);
							break;
						}
					}
				}
			}
		}
	}
}

#pragma clang diagnostic pop

void Server::ProcessPacket(sf::TcpSocket &clientSocket, sf::Packet packet)
{
	std::cout << "[SERVER] Received packet from client: " << clientSocket.getRemoteAddress() << std::endl;
	sf::Int8 commandTypeInt;
	packet >> commandTypeInt;
	
	auto commandType = static_cast<CommandType>(commandTypeInt);
	switch (commandType)
	{
		case CommandType::Connected:
		{
			if (connections.size() >= Game::MAX_PLAYERS)
			{
				std::cout << "There is a maxim number of players." << std::endl;
				
				SendRejectedClientCommand(&clientSocket);
				return;
			}
			
			auto *player = new Player();
			packet >> *player;
			
			std::string playerName = player->GetName();
			for (auto currentConnection: connections)
			{
				if (currentConnection->getPlayer()->GetName() == playerName)
				{
					std::cout << "This name already exists (" << playerName << ")" << std::endl;
					
					SendRejectedClientCommand(&clientSocket);
					return;
				}
			}
			
			for (auto currentConnection: connections)
			{
				if (currentConnection->getSocket() == &clientSocket)
				{
					currentConnection->setPlayer(player);
				}
			}
			
			std::cout << "Received connected command: " << player->GetName() << ", Role: " << player->GetRoleName()
			          << std::endl;
			
			SendPlayerConnectedCommand(*player, &clientSocket);
			SendPlayerListCommand(&clientSocket);
			break;
		}
		case CommandType::Disconnected:
		{
			std::string name;
			packet >> name;
			if (RemovePlayerWithName(name))
			{
				std::cout << "Received disconnected command: " << name << std::endl;
				
				socketSelector->remove(clientSocket);
				
				SendPlayerDisconnectedCommand(name);
			}
			break;
		}
		case CommandType::StartGame:
		{
			std::cout << "[ERROR] Server received Start Game command. That's not supposed to happen." << std::endl;
			break;
		}
		case CommandType::PlayerList:
		{
			std::cout << "[ERROR] Server received Players List command. That's not supposed to happen." << std::endl;
			break;
		}
		case CommandType::Rejected:
		{
			std::cout << "[ERROR] Server received Rejected command. That's not supposed to happen." << std::endl;
			break;
		}
		case CommandType::RoleChange:
		{
			std::string playerName;
			packet >> playerName;
			
			sf::Int8 playerRole;
			packet >> playerRole;
			
			for (auto connection: connections)
			{
				if (connection->getPlayer()->GetName() == playerName)
				{
					connection->getPlayer()->SetRole(static_cast<PlayerRole>(playerRole));
					break;
				}
			}
			
			SendRoleChangeCommand(&clientSocket, playerName, playerRole);
			break;
		}
		case CommandType::VisionCards:
		{
			sf::Uint8 psychicID;
			packet >> psychicID;
			
			sf::Uint8 size;
			packet >> size;
			
			std::cout << "[SERVER] VisionCards size: " << (int) size << std::endl;
			
			std::set<uint8_t> cardsIDs;
			for (unsigned int index = 0; index < size; index++)
			{
				sf::Uint8 ID;
				packet >> ID;
				cardsIDs.insert((uint8_t) ID);
			}
			
			for (auto connection : connections)
			{
				if (connection->getPsychicId() == (uint16_t) psychicID)
				{
					SendVisionCardsCommand(connection->getSocket(), (uint8_t) size, cardsIDs);
				}
			}
			std::cout << "[SERVER] Sent vision cards to the psychic " << (int) psychicID << ".\n";
			break;
		}
		case CommandType::Advance:
		{
			sf::Uint8 guessed;
			packet >> guessed;
			
			sf::Packet ghostPacket;
			ghostPacket << (sf::Int8) CommandType::GhostGuessInfo;
			for (auto connection: connections)
			{
				if (connection->getPlayer()->GetRole() == PlayerRole::Ghost_Role)
				{
					ghostPacket << (sf::Uint8) connection->getPsychicId();
					ghostPacket << (sf::Uint8) guessed;
					connection->getSocket()->send(ghostPacket);
				}
			}
			
			guessedCount++;
			if (guessedCount >= connections.size() - 1)
			{
				SendAdvanceCommand();
				guessedCount = 0;
			}
			break;
		}
		default:
		{
			std::cout << "[SERVER] Invalid command type." << std::endl;
		}
	}
}

void Server::SendStartGameCommand()
{
	if (!CanStartGame())
	{
		std::cout << "[SERVER] Tried to send StartGame command, but the verification failed." << std::endl;
		return;
	}
	
	sf::TcpSocket *ghostSocket;
	for (auto conn: connections)
	{
		if (conn->getPlayer()->GetRole() == PlayerRole::Ghost_Role)
		{
			ghostSocket = conn->getSocket();
		}
	}
	
	std::vector<uint8_t> charactersVector;
	std::vector<uint8_t> locationsVector;
	std::vector<uint8_t> objectsVector;
	
	for (uint8_t index = 0; index < Game::MAX_CHARACTER_CARDS; index++)
	{
		charactersVector.push_back(index);
	}
	
	for (uint8_t index = 0; index < Game::MAX_LOCATION_CARDS; index++)
	{
		locationsVector.push_back(index);
	}
	
	for (uint8_t index = 0; index < Game::MAX_OBJECT_CARDS; index++)
	{
		objectsVector.push_back(index);
	}
	
	sf::Packet packet;
	sf::Uint8 psychicIndex = 1;
	for (auto connection: connections)
	{
		if (connection->getPlayer()->GetRole() == PlayerRole::Psychic_Role)
		{
			connection->setPsychicId(psychicIndex);
			
			sf::Uint8 characterIndex = rand() % (charactersVector.size() - 1);
			sf::Uint8 locationIndex = rand() % (locationsVector.size() - 1);
			sf::Uint8 objectIndex = rand() % (objectsVector.size() - 1);
			
			packet.clear();
			packet << (sf::Int8) CommandType::StartGame;
			
			packet << psychicIndex;
			packet << charactersVector.at(characterIndex);
			packet << locationsVector.at(locationIndex);
			packet << objectsVector.at(objectIndex);
			
			connection->getSocket()->send(packet);
			ghostSocket->send(packet);
			std::cout << "[SERVER] Sent clues for psychic: " << (int) psychicIndex << ": "
			          << (int) charactersVector.at(characterIndex)
			          << (int) locationsVector.at(locationIndex) << (int) objectsVector.at(objectIndex) << std::endl;
			
			charactersVector.erase(charactersVector.begin() + characterIndex);
			locationsVector.erase(locationsVector.begin() + locationIndex);
			objectsVector.erase(objectsVector.begin() + objectIndex);
			
			psychicIndex++;
		}
	}
	
	sf::Packet ghostPacket;
	ghostPacket << (sf::Int8) CommandType::GhostStart;
	ghostSocket->send(ghostPacket);
	
	std::cout << "Sent start game command." << std::endl;
}

void Server::SendPlayerConnectedCommand(const Player &player, sf::TcpSocket *playerSocket)
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::Connected;
	packet << player;
	
	for (auto *connection: connections)
	{
		if (connection->getSocket() != playerSocket)
		{
			connection->getSocket()->send(packet);
		}
	}
	
	std::cout << "Sent connected command to all sockets: " << player.GetName() << ", Role: " << player.GetRoleName()
	          << std::endl;
}

void Server::SendAdvanceCommand()
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::Advance;
	
	for (auto *connection: connections)
	{
		connection->getSocket()->send(packet);
	}
}

void Server::SendPlayerListCommand(sf::TcpSocket *socket)
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::PlayerList;
	packet << (sf::Uint8) connections.size();
	
	for (auto connection: connections)
	{
		packet << *connection->getPlayer();
	}
	
	socket->send(packet);
	std::cout << "[SERVER] Sent player list command." << std::endl;
}

void Server::SendPlayerDisconnectedCommand(const std::string &name)
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::Disconnected;
	packet << name;
	
	for (auto *connection: connections)
	{
		connection->getSocket()->send(packet);
	}
	
	std::cout << "[SERVER] Sent disconnected command to all sockets: " << name << std::endl;
}

bool Server::CanStartGame()
{
	if (Game::instance->GetPlayersNumber() < 1)
	{
		return false;
	}
	
	uint8_t ghostsNumber = 0;
	for (auto connection: connections)
	{
		if (connection->getPlayer()->GetRole() == None_Role)
		{
			return false;
		}
		else
		{
			if (connection->getPlayer()->GetRole() == Ghost_Role)
			{
				ghostsNumber++;
			}
		}
	}
	
	if (ghostsNumber != 1)
	{
		return false;
	}
	
	return true;
}

bool Server::RemovePlayerWithName(const std::string &name)
{
	for (auto it = connections.begin(); it != connections.end(); it++)
	{
		if ((*it)->getPlayer()->GetName() == name)
		{
			connections.erase(it);
			return true;
		}
	}
	return false;
}

bool Server::isValidIPAddress(const std::string &ipAddress)
{
	auto i = 0;
	std::vector<std::string> splitIPAddress;
	
	auto pos = ipAddress.find('.');
	
	while (pos != std::string::npos)
	{
		splitIPAddress.push_back(ipAddress.substr(i, pos - i));
		i = ++pos;
		pos = ipAddress.find('.', pos);
	}
	
	splitIPAddress.push_back(ipAddress.substr(i, ipAddress.length()));
	
	if (splitIPAddress.size() != 4)
	{
		return false;
	}
	
	for (const auto &str : splitIPAddress)
	{
		if (!isNumber(str) || stoi(str) > 255 || stoi(str) < 0)
		{
			return false;
		}
	}
	return true;
}

bool Server::isNumber(const std::string &string)
{
	return !string.empty() && (string.find_first_not_of("[0123456789]") == std::string::npos);
}

void Server::SendRejectedClientCommand(sf::TcpSocket *socket)
{
	sf::Packet packet;
	packet << (sf::Int8) CommandType::Rejected;
	
	socket->send(packet);
}

void Server::SendRoleChangeCommand(sf::TcpSocket *socket, const std::string &playerName, sf::Int8 role)
{
	sf::Packet packet;
	
	packet << (sf::Int8) CommandType::RoleChange;
	packet << playerName;
	packet << role;
	
	for (auto connection: connections)
	{
		if (connection->getSocket() != socket)
		{
			connection->getSocket()->send(packet);
		}
	}
}

void Server::SendVisionCardsCommand(sf::TcpSocket *psychicSocket, unsigned int size, const std::set<uint8_t> &cards)
{
	sf::Packet packet;
	
	packet << (sf::Int8) CommandType::VisionCards;
	
	packet << (sf::Uint8) size;
	
	for (auto card: cards)
	{
		packet << (sf::Uint8) card;
	}
	
	psychicSocket->send(packet);
}

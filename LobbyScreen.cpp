#include "LobbyScreen.h"
#include "Psychic.h"
#include "Log.h"

std::vector<TextField *> LobbyScreen::texts;
sf::Font *LobbyScreen::fontMagnificent;

LobbyScreen::LobbyScreen()
{
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;
	
	auto widthButton = width / 6.6f;
	auto heightButton = height / 10.8f;
	
	auto characterSize = height / 27;
	
	// gap between buttons
	auto gap = height / 39.6f;
	
	m_currentPlayer = new Player();
	
	fontMagnificent = new sf::Font();
	fontMagnificent->loadFromFile("Resources/Fonts/Magnificent.ttf");
	
	auto *backButton = new Button(width - widthButton - gap, height - heightButton - gap, widthButton, heightButton,
	                              fontMagnificent, characterSize, "BACK",
	                              sf::Color(150, 150, 150), sf::Color::White);
	
	std::function<void()> BackButtonFunction = []()
	{
		Game::instance->GetClient()->Disconnect();
		
		if (Game::instance->IsServerActive())
		{
			Game::instance->CloseServer();
		}
		
		Game::instance->getGraphicHandler()->setTargetScreen(new MainMenuScreen());
	};
	
	backButton->addActionOnPress((BackButtonFunction));
	buttons.insert(backButton);
	
	
	auto *roleButton = new Button(width - widthButton - gap, height - heightButton * 2 - gap, widthButton, heightButton,
	                              fontMagnificent, characterSize, "ROLE",
	                              sf::Color(150, 150, 150), sf::Color::White);
	
	std::function<void()> RoleButtonFunction = [this]()
	{
		Log::logger.RoleChanged(__LINE__);
		switch (m_currentPlayer->GetRole())
		{
			case None_Role:
				m_currentPlayer->SetRole(PlayerRole::Ghost_Role);
				break;
			case Ghost_Role:
				m_currentPlayer->SetRole(PlayerRole::Psychic_Role);
				break;
			case Psychic_Role:
				m_currentPlayer->SetRole(PlayerRole::Ghost_Role);
				break;
			default:
				break;
		}
		Game::instance->GetPlayer()->SetRole(m_currentPlayer->GetRole());
		Game::instance->GetClient()->SendRoleChangeCommand();
		
		redrawPlayers();
	};
	
	roleButton->addActionOnPress((RoleButtonFunction));
	buttons.insert(roleButton);
	
	
	auto *startButton = new Button(width - widthButton * 2 - gap, height - heightButton - gap, widthButton,
	                               heightButton, fontMagnificent, characterSize, "START",
	                               sf::Color(150, 150, 150), sf::Color::White);
	
	std::function<void()> StartButtonFunction = [=]()
	{
		if (!Game::instance->IsServerActive())
			return;
		
		Game::instance->GetServer()->SendStartGameCommand();
	};
	
	startButton->addActionOnPress((StartButtonFunction));
	buttons.insert(startButton);
	
	
	players = new TextField(width / 2.f - 100, height / 3.2f, 200, 60, fontMagnificent, 50,
	                        "PLAYERS (" + std::to_string(Game::instance->GetPlayersNumber()) + '/'
	                        + std::to_string(Game::instance->GetMaxPlayers()) + ')', sf::Color(200, 214, 244));
	
	
	for (auto player : Game::instance->getPlayersList())
	{
		if (Game::instance->IsCurrentPlayer(player))
		{
			m_currentPlayer = player;
			break;
		}
	}
	
	redrawPlayers();
}

LobbyScreen::~LobbyScreen()
= default;

void LobbyScreen::updateFrame()
{

}

void LobbyScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);
	
	players->EditText("PLAYERS (" + std::to_string(Game::instance->GetPlayersNumber()) + '/'
	                  + std::to_string(Game::instance->GetMaxPlayers()) + ')');
	players->renderTextField(window);
	
	for (auto &text : texts)
		text->renderTextField(window);
}

void LobbyScreen::redrawPlayers()
{
	for (auto *text: texts)
	{
		delete text;
	}
	
	texts.clear();
	
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;
	
	for (int index = 0; index < Game::instance->GetPlayersNumber(); index++)
	{
		std::string newText = Game::instance->getPlayersList().at(index)->GetName() + " (" +
		                      Game::instance->getPlayersList().at(index)->GetRoleName() + ')';
		
		auto *newTextField = new TextField(width / 2.f - 100, height / 2.57f + (float) index * 50,
		                                   200, 50, fontMagnificent, 40, newText,
		                                   sf::Color(165, 214, 244));
		
		if (Game::instance->IsCurrentPlayer(Game::instance->getPlayersList().at(index)))
			newTextField->SetColor(sf::Color(180, 70, 70));
		
		texts.push_back(newTextField);
	}
}

sf::Font *LobbyScreen::GetFont()
{
	return this->fontMagnificent;
}

unsigned int LobbyScreen::GetPlayersCharacterSize()
{
	return this->players->GetTextSize();
}

sf::Color LobbyScreen::GetPlayersColor()
{
	return this->players->GetTextColor();
}

float LobbyScreen::GetTextWidth()
{
	return this->players->GetWidth();
}

float LobbyScreen::GetTextHeight()
{
	return this->players->GetHeight();
}

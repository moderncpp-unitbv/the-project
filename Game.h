#ifndef MYSTERIUMGAME_GAME_H
#define MYSTERIUMGAME_GAME_H

#include <iostream>
#include <vector>
#include <functional>
//#include <gtest/gtest.h>
//#include <gmock/gmock.h>
#include "SFML/Graphics.hpp"
#include "CharacterCard.h"
#include "LocationCard.h"
#include "ObjectCard.h"
#include "VisionCard.h"
#include "TimeHandler.h"
#include "Psychic.h"
#include "Button.h"
#include "MainMenuScreen.h"
#include "InputHandler.h"
#include "Player.h"
#include "Clock.h"

class GraphicHandler;

class Server;

class Client;

class Player;

class Game
{
public:
	
	// constants
	static const int CONNECTION_PORT = 63444;
	
	static const int MAX_CHARACTER_CARDS = 4;
	static const int MAX_LOCATION_CARDS = 4;
	static const int MAX_OBJECT_CARDS = 4;
	static const int MAX_VISION_CARDS = 20;

	static const int MAX_PLAYERS = 5;

public:
	
	// DECKS
	std::vector<CharacterCard *> GetCharacterDeck() const;
	
	std::vector<LocationCard *> GetLocationDeck() const;
	
	std::vector<ObjectCard *> GetObjectDeck() const;
	
	std::vector<VisionCard *> GetVisionDeck() const;
	
	// HANDLERS
	GraphicHandler *getGraphicHandler() const;
	
	TimeHandler *getTimeHandler() const;
	
	InputHandler *getInputHandler() const;
	
	// PLAYERS
	class Player *GetPlayer() const;
	
	std::vector<class Player *> getPlayersList();
	
	uint8_t GetPlayersNumber();
	
	bool IsCurrentPlayer(Player *) const;
	
	// SERVER
	void InitServer();
	
	void CloseServer();
	
	bool IsServerActive();
	
	Server *GetServer() const;
	
	// CLIENT
	void InitClient();
	
	Client *GetClient() const;
	
	template<class T>
	void RemoveFromVector(std::vector<T> &vector, const T &value);
	
	void RemoveIndexFromCharacterDeck(int index);
	
	void RemoveIndexFromLocationDeck(int index);
	
	void RemoveIndexFromObjectDeck(int index);
	
	void RemoveIndexFromVisionDeck(int index);
	
	void AddToVisionDeck(VisionCard *);
	
	void addMouseMovedFunction(std::function<bool(sf::Vector2i)> *pFunction);
	
	void addMousePressedFunction(std::function<bool(sf::Vector2i, sf::Mouse::Button)> *pFunction);
	
	void removeMouseMovedFunctionIndex(std::function<bool(sf::Vector2i)> *pFunction);
	
	void removeMousePressedFunctionIndex(std::function<bool(sf::Vector2i, sf::Mouse::Button)> *pFunction);
	
	void updateGameFrame();
	
	void AddPlayer(Player *newPlayer);
	
	void RemovePlayerByName(const std::string &name);
	
	void ClearPlayersList();
	
	void RedrawPlayerList();
	
	void SetMaxPlayers(uint8_t maxPlayers);// the host should decide the lobby size
	
	std::vector<uint8_t> GenerateCardSet();
	
	uint8_t GetMaxPlayers() const;
	
	Clock* GetClock() const;

public:
	
	Game();
	
	virtual ~Game();

private:
	std::vector<std::function<bool(sf::Vector2i)> *> onMouseMovedFunctions;
	std::vector<std::function<bool(sf::Vector2i, sf::Mouse::Button)> *> onMousePressedFunctions;
	
	std::vector<CharacterCard *> m_characterDeck;
	std::vector<LocationCard *> m_locationDeck;
	std::vector<ObjectCard *> m_objectDeck;
	std::vector<VisionCard *> m_visionDeck;
	
	std::vector<Player *> PlayersList;
	
	GraphicHandler *graphicHandler;
	TimeHandler *timeHandler;
	InputHandler *inputHandler;
	
	Client *m_client;
	Server *m_server;
	Player *m_player;
	
	Clock *m_clock;
	
	uint8_t MaxPlayers = 8;

public:
	inline static Game *instance;

public:
	void RunTests();
};

#endif //MYSTERIUMGAME_GAME_H

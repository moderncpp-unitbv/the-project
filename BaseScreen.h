#ifndef MYSTERIUMGAME_BASESCREEN_H
#define MYSTERIUMGAME_BASESCREEN_H

#include <iostream>
#include <set>
#include "Button.h"
#include "InputField.h"
#include "TextField.h"
#include "CardButton.h"

class BaseScreen
{

public:
	
	BaseScreen() = default;
	
	virtual ~BaseScreen()
	{
		for (Button *button: buttons)
		{
			delete button;
		}
		buttons.clear();
		
		for (InputField *inputField: inputFields)
		{
			delete inputField;
		}
		inputFields.clear();
	}
	
	virtual void updateFrame() = 0;
	
	virtual void renderScreenFrame(sf::RenderWindow *window)
	{
		for (Button *button: buttons)
		{
			button->renderButton(window);
		}
		for (InputField *inputField: inputFields)
		{
			inputField->renderInputField(window);
		}
		for (TextField *textField: textFields)
		{
			textField->renderTextField(window);
		}
	}

protected:
	
	std::set<Button *> buttons;
	std::set<InputField *> inputFields;
	std::set<TextField *> textFields;
};


#endif //MYSTERIUMGAME_BASESCREEN_H

#include "gtest/gtest.h"
#include "SFML/include/SFML/Graphics.hpp"
#include "InputField.h"

class InputFieldTest : public ::testing::Test
{
protected:
    void SetUp() override
    {}

    void TearDown() override
    {}
};

TEST_F(InputFieldTest, InputFieldExist)
{
    sf::Font *font = new sf::Font();
    font->loadFromFile("Resources/Fonts/Magnificent.ttf");

    InputField *newInputField = new InputField(0, 0, 300, 200, font, 40, 0);

    EXPECT_GE(newInputField->GetXPosition(), 0);

    EXPECT_GE(newInputField->GetYPosition(), 0);

    EXPECT_GT(newInputField->GetWidth(), 0);

    EXPECT_GT(newInputField->GetHeight(), 0);

    EXPECT_GT(newInputField->GetCharacterSize(), 0);
}
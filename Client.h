#ifndef MYSTERIUMGAME_CLIENT_H
#define MYSTERIUMGAME_CLIENT_H

#include <iostream>
#include "SFML/System.hpp"
#include "SFML/Network.hpp"
#include "Game.h"
#include "CommandType.h"
#include "Player.h"

class Client
{
public:
	Client();
	
	virtual ~Client();
	
	void StartConnectThread(sf::IpAddress ipAddress);
	
	void Connect(sf::IpAddress targetIpAddress);
	
	void Disconnect();
	
	void ReadPacket(sf::Packet receivedPacket);
	
	void SendConnectedCommand();
	
	void SendDisconnectedCommand();

	void SendRoleChangeCommand();
	
	void SendAdvanceCommand(uint8_t guessed);
	
	bool IsConnected() const;

	void SendVisionCardsCommand(uint8_t PsychicID, const std::vector<VisionCard*>& cards);

private:
	sf::TcpSocket *socket;
	bool connected;
	sf::Thread *clientConnectThread;
};


#endif //MYSTERIUMGAME_CLIENT_H

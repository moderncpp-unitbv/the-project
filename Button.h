#ifndef MYSTERIUMGAME_BUTTON_H
#define MYSTERIUMGAME_BUTTON_H


#include <functional>
#include <thread>
#include "SFML/Graphics.hpp"

class Button
{
private:
	sf::Sprite *m_sprite;
	sf::Texture *m_texture;
	sf::Text *m_text;
	sf::Font m_font;
	int m_characterSize;
	
	sf::Color m_idleColor;
	sf::Color m_hoverColor;
	sf::Color m_activeColor;
	
	bool m_hovering;
	
	std::vector<std::function<void()>> actionsOnPress;
	
	std::function<bool(sf::Vector2i)> mouseMovedFunction;
	std::function<bool(sf::Vector2i, sf::Mouse::Button)> mousePressedFunction;
	std::function<void()> colorBackFunction;

public:
	Button(float x, float y, float width, float height, sf::Font *font, int characterSize, const std::string &text,
	       sf::Color hoverColor, sf::Color activeColor);
	
	~Button();

public:
	void renderButton(sf::RenderWindow *window);
	
	bool onMouseHover(sf::Vector2i);
	
	bool onMousePressed(sf::Vector2i, sf::Mouse::Button);
	
	void pressButton();
	
	void addActionOnPress(const std::function<void()> &pFunction);
	
	void removeActionOnPressIndex(int index);
	
	void SetIdleColor(const sf::Color&);

	float GetXPosition();

	float GetYPosition();

	float GetWidth();

	float GetHeight();

	int GetCharacterSize();

	sf::Color GetColor();
	
};


#endif //MYSTERIUMGAME_BUTTON_H

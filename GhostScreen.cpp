#include <iostream>
#include "GhostScreen.h"
#include "Client.h"
#include "Log.h"

std::vector<TextField *> GhostScreen::textFieldGuessing;

GhostScreen::GhostScreen()
{
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;
	
	auto widthButton = width / 6.6f;
	auto heightButton = height / 10.8f;
	
	// gap between buttons
	auto gap = height / 21.6f;
	auto characterSize = height / 27;
	
	sf::Font fontMagnificent;
	fontMagnificent.loadFromFile("Resources/Fonts/Magnificent.ttf");
	
	Game::instance->getGraphicHandler()->SetTexture("background", "Resources/Textures/Backgrounds/board.jpg");
	
	// VISION CARDS
	for (unsigned int index = 0; index < 6; index++)
	{
		auto newCard = Game::instance->GetVisionDeck().at(0);
		newCard->ResizeTexture();
		if (index % 2 == 0)
		{
			newCard->SetPosition(1920 - 355.0f, 100.0f + index * 120.0f);
			auto visionButton = new CardButton(width - 355.0f, 100.0f + index * 120.0f, 250 * 0.6f - 9, 355 * 0.6f,
			                                   newCard);
			visionButton->SetVisible(true);
			cardButtons.insert(visionButton);
		}
		if (index % 2 == 1)
		{
			newCard->SetPosition(width - 200.0f, 100.0f + (index - 1) * 120.0f);
			auto visionButton = new CardButton(width - 200.0f, 100.0f + (index - 1) * 120.0f, 250 * 0.6f - 9,
			                                   355 * 0.6f, newCard);
			visionButton->SetVisible(true);
			cardButtons.insert(visionButton);
		}
		visionCards.push_back(newCard);
		Game::instance->RemoveIndexFromVisionDeck(0);
	}
	
	for (uint8_t index = 0; index < Game::instance->GetPlayer()->getGhost()->getPsychics().size(); index++)
	{
		auto psychic = Game::instance->GetPlayer()->getGhost()->getPsychics().at(index);
		auto i = psychic->GetIndex();
		
		auto characterCard = psychic->GetCardSet()->getCharacterCard();
		auto locationCard = psychic->GetCardSet()->getLocationCard();
		auto objectCard = psychic->GetCardSet()->getObjectCard();
		
		characterCard->ResizeTexture();
		locationCard->ResizeTexture();
		objectCard->ResizeTexture();
		
		characterCard->SetPosition(400.0f, 250);
		locationCard->SetPosition(400.0f, 600);
		objectCard->SetPosition(900.0f, 400);
		
		auto *buttonChoosePsychic = new Button(50, (float) i * 100 + 200, 130, 70,
		                                       &fontMagnificent, characterSize, std::to_string(i),
		                                       sf::Color(150, 150, 150), sf::Color::White);
		
		textFieldGuessing.push_back(new TextField(180, (float) i * 100 + 205, 150, 70,
		                                          &fontMagnificent, characterSize, "waiting",
		                                          sf::Color(165, 214, 244)));
		
		std::function<void()> ButtonFunction = [this, psychic, buttonChoosePsychic]()
		{
			if (psychic->HasReceivedVisions())
				std::cout << "This psychic has already received visions.\n";
			else
			{
				Log::logger.GhostChangedPsychic(__LINE__);
				if (!m_currentPsychic->HasReceivedVisions())
					m_currentButton->SetIdleColor(sf::Color::White);
				
				m_currentPsychic = psychic;
				m_currentButton = buttonChoosePsychic;
				m_currentButton->SetIdleColor(sf::Color(130, 130, 230));
				
			}
		};
		buttonChoosePsychic->addActionOnPress(ButtonFunction);
		buttons.insert(buttonChoosePsychic);
		textFields.insert(textFieldGuessing[index]);
		
		m_currentPsychic = psychic;
		m_currentButton = buttonChoosePsychic;
		m_currentButton->SetIdleColor(sf::Color(130, 130, 230));
	}
	
	auto *text = new TextField(35, 100, 300, 50, &fontMagnificent, 40,
	                           "CHOOSE A PSYCHIC", sf::Color(165, 214, 244));
	textFields.insert(text);
	
	auto *give = new Button(width - widthButton - 20, height - heightButton - 20, widthButton, heightButton,
	                        &fontMagnificent, characterSize, "GIVE", sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> GiveButtonFunction = [this]()
	{
		Log::logger.GhostGaveCards(__LINE__);
		for (auto visionButton : cardButtons)
		{
			if (visionButton->IsPressed())
				m_selectedVisionCards.push_back(dynamic_cast<VisionCard *>(visionButton->GetCard()));
		}
		if (m_selectedVisionCards.size() > Game::instance->GetVisionDeck().size())
		{
			std::cout << "ERROR. There aren't as many vision cards in the initial deck as you selected.\n";
			std::cout << "Please select only " << std::to_string(Game::instance->GetVisionDeck().size()) << " cards.\n";
		}
		else if (!m_selectedVisionCards.empty())
		{
			Game::instance->GetClient()->SendVisionCardsCommand(m_currentPsychic->GetIndex(), m_selectedVisionCards);
			m_currentButton->SetIdleColor(sf::Color(130, 230, 130));
			for (auto visionButton : cardButtons)
			{
				if (visionButton->IsPressed())
				{
					auto visionCard = RollVisionCard(dynamic_cast<VisionCard *>(visionButton->GetCard()), true);
					visionButton->SetCard(visionCard);
				}
			}
		}
		m_selectedVisionCards.clear();
		for (auto cardButton : cardButtons)
		{
			cardButton->Reset();
		}
	};
	give->addActionOnPress(GiveButtonFunction);
	buttons.insert(give);
	
//	auto *endTurn = new Button(width - 2 * widthButton - 20, height - heightButton - 20, widthButton, heightButton,
//	                           &fontMagnificent, characterSize, "END TURN", sf::Color(150, 150, 150), sf::Color::White);
//	std::function<void()> EndTurnButtonFunction = [this]()
//	{
//		bool ok = true;
//		for (auto psychic : Game::instance->GetPlayer()->getGhost()->getPsychics())
//		{
//			if (!psychic->HasReceivedVisions())
//			{
//				ok = false;
//				break;
//			}
//		}
//		if (ok)
//			Log::logger.GhostEndedTurn(__LINE__);
//		else
//			Log::logger.CardsNotGiven(__LINE__);
//	};
//	endTurn->addActionOnPress(EndTurnButtonFunction);
//	buttons.insert(endTurn);
//
//	m_currentPsychic = *Game::instance->GetPlayer()->getGhost()->getPsychics().begin();
//	m_currentButton = *buttons.begin();
//	m_currentButton->SetIdleColor(sf::Color(130, 130, 230));
}

GhostScreen::~GhostScreen()
= default;

void GhostScreen::updateFrame()
{
}

void GhostScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);
	
	for (auto button : cardButtons)
	{
		button->renderButton(window);
	}
	
	for (auto button : buttons)
	{
		button->renderButton(window);
	}
	
	for (auto textField : textFields)
	{
		textField->renderTextField(window);
	}
	
	for (auto visionCard : visionCards)
	{
		window->draw(*visionCard->GetSprite());
	}
	
	window->draw(*m_currentPsychic->GetCardSet()->getCharacterCard()->GetHighlight());
	
	window->draw(*m_currentPsychic->GetCardSet()->getCharacterCard()->GetSprite());
	window->draw(*m_currentPsychic->GetCardSet()->getLocationCard()->GetSprite());
	window->draw(*m_currentPsychic->GetCardSet()->getObjectCard()->GetSprite());
	window->draw(*m_currentPsychic->GetGhostToken());
}

VisionCard *GhostScreen::RollVisionCard(VisionCard *vision, bool givenToPsychic)
{
	auto position = vision->GetSprite()->getPosition();
	
	// IN CASE THE GHOST REROLLED IT WITH A CROW
	if (!givenToPsychic)
		Game::instance->AddToVisionDeck(vision);
	
	auto newVisionCard = Game::instance->GetVisionDeck().front();
	Game::instance->RemoveIndexFromVisionDeck(0);
	newVisionCard->SetPosition(position.x, position.y);
	newVisionCard->ResizeTexture();
	
	auto it = std::remove(visionCards.begin(), visionCards.end(), vision);
	visionCards.erase(it);
	visionCards.insert(it, newVisionCard);
	
	return newVisionCard;
}

unsigned int GhostScreen::GetVisionCardsSize()
{
	return this->visionCards.size();
}

float GhostScreen::GetWidth()
{
	return Game::instance->getGraphicHandler()->window->getSize().x;
}

float GhostScreen::GetHeight()
{
	return Game::instance->getGraphicHandler()->window->getSize().y;
}

TextField *GhostScreen::getTextFieldGuessing(uint8_t index)
{
	return textFieldGuessing.at(index);
}

void GhostScreen::setTextFieldGuessingText(int index, const std::string &newText)
{
	GhostScreen::textFieldGuessing.at((int) index)->EditText(newText);
}

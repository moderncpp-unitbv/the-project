#include "SoundHandler.h"
#include <iostream>

SoundHandler::SoundHandler()
{
	if (instance != nullptr)
	{
		delete instance;
	}
	instance = this;
	
	theme = std::make_unique<sf::Music>();
	if (!theme->openFromFile("Resources/Sounds/theme.ogg"))
		std::cout << "Couldn't find the theme music.";
	theme->setLoop(true);
	theme->play();
	isPlaying = true;
	
	if (!click.first.loadFromFile("Resources/Sounds/click.ogg"))
		std::cout << "Couldn't find the click sound effect.";
	click.second.setBuffer(click.first);
}

void SoundHandler::toggleMusic()
{
	if (isPlaying)
	{
		theme->stop();
	}
	else
	{
		theme->play();
	}
	isPlaying = !isPlaying;
}

void SoundHandler::playClickSound()
{
	click.second.play();
}

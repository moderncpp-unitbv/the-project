
#include "Game.h"
#include "ExitScreen.h"
#include "Log.h"

ExitScreen::ExitScreen()
{
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;
	
	auto widthButton = width / 6.6f;
	auto heightButton = height / 10.8f;
	
	auto widthTextField = width / 2.7f;
	auto heightTextField = height / 10.8f;
	
	auto characterSize = height / 27;
	
	// gap between buttons
	auto gap = height / 21.6f;
	
	sf::Font fontMagnificent;
	fontMagnificent.loadFromFile("Resources/Fonts/Magnificent.ttf");
	
	auto *exitField = new TextField((width - widthTextField) / 2.0f , height / 2.0f - heightTextField - gap,
								 widthTextField, heightTextField,&fontMagnificent, characterSize,
								 "DO YOU WANT TO CLOSE THE GAME?", sf::Color(165, 214, 244));
	textFields.insert(exitField);
	
	gap=gap-heightButton*2.0f;
	
	auto *BackToMainMenu = new Button((width - widthButton) / 1.6f, height / 2.f - heightButton - gap, widthButton, heightButton,
	                                    &fontMagnificent, characterSize, "NO",
	                                    sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> BackToMainMenuFunction = []()
	{
		Game::instance->getGraphicHandler()->setTargetScreen(new MainMenuScreen());
	};
	BackToMainMenu->addActionOnPress(BackToMainMenuFunction);
	buttons.insert(BackToMainMenu);
	
	
	
	auto *Exit = new Button((width - widthButton) / 2.6f, height / 2.f - heightButton - gap, widthButton, heightButton,
	                                  &fontMagnificent, characterSize, "YES",
	                                  sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()>ExitFunction = []()
	{
		Game::instance->getGraphicHandler()->window->close();
		Log::logger.PlayerQuit(__LINE__);
	};
	Exit->addActionOnPress(ExitFunction);
	buttons.insert(Exit);
	
}

ExitScreen::~ExitScreen()
= default;

void ExitScreen::updateFrame()
{
}

void ExitScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);

}



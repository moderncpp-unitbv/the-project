#include "Button.h"
#include "Game.h"
#include "SoundHandler.h"

Button::Button(float x, float y, float width, float height, sf::Font *font, int characterSize, const std::string &text,
			   sf::Color hoverColor, sf::Color activeColor)
{
	this->m_sprite = new sf::Sprite();
	this->m_text = new sf::Text();
	this->m_texture = new sf::Texture;
	
	this->m_texture->loadFromFile("Resources/Textures/Buttons/button.png");
	
	m_sprite->setTexture(*m_texture);
	m_sprite->setPosition(x, y);
	sf::Vector2u textureSize = m_sprite->getTexture()->getSize();
	float scaleX = (float) width / textureSize.x;
	float scaleY = (float) height / textureSize.y;
	m_sprite->setScale(scaleX, scaleY);
	m_idleColor = sf::Color::White;
	m_sprite->setColor(m_idleColor);
	
	this->m_font = *font;
	this->m_text->setFont(this->m_font);
	this->m_text->setString(text);
	this->m_text->setFillColor(sf::Color::Black);
	this->m_text->setOutlineColor(sf::Color(165, 214, 244));
	this->m_text->setOutlineThickness(2);
	this->m_text->setCharacterSize(characterSize);
	this->m_text->setPosition(this->m_sprite->getPosition().x + this->m_sprite->getGlobalBounds().width / 2.0f -
	                          this->m_text->getGlobalBounds().width / 2.0f,
	                          this->m_sprite->getPosition().y + this->m_sprite->getGlobalBounds().height / 2.0f -
	                          this->m_text->getGlobalBounds().height);
	
	this->m_characterSize = characterSize;
	this->m_hoverColor = hoverColor;
	this->m_activeColor = activeColor;
	m_hovering = false;
	
	mouseMovedFunction = [=](sf::Vector2i position)
	{
		return this->onMouseHover(position);
	};
	
	mousePressedFunction = [=](sf::Vector2i position, sf::Mouse::Button button)
	{
		return this->onMousePressed(position, button);
	};
	
	Game::instance->addMouseMovedFunction(&mouseMovedFunction);
	Game::instance->addMousePressedFunction(&mousePressedFunction);
}

Button::~Button()
{
	Game::instance->removeMouseMovedFunctionIndex(&mouseMovedFunction);
	Game::instance->removeMousePressedFunctionIndex(&mousePressedFunction);
	Game::instance->getTimeHandler()->removeTimer(&colorBackFunction);
}

bool Button::onMouseHover(sf::Vector2i position)
{
	if (this->m_sprite->getGlobalBounds().contains(position.x, position.y - m_texture->getSize().y / 3.f))
	{
		if (!m_hovering)
		{
			m_hovering = true;
			this->m_sprite->setColor(m_hoverColor);
		}
	}
	else
	{
		if (m_hovering)
		{
			m_hovering = false;
			this->m_sprite->setColor(m_idleColor);
		}
	}
	return false;
}

bool Button::onMousePressed(sf::Vector2i position, sf::Mouse::Button button)
{
	if (button == sf::Mouse::Button::Left)
	{
		if (this->m_sprite->getGlobalBounds().contains(position.x, position.y - m_texture->getSize().y / 3.f))
		{
			m_hovering = false;
			this->m_sprite->setColor(m_activeColor);
			
			colorBackFunction = [=]()
			{
				this->onMouseHover(sf::Mouse::getPosition());
			};
			Game::instance->getTimeHandler()->setTimer(&colorBackFunction, 0.1f);
			
			pressButton();
			SoundHandler::instance->playClickSound();
			
			return true;
		}
	}
	return false;
}

void Button::addActionOnPress(const std::function<void()> &pFunction)
{
	actionsOnPress.push_back(pFunction);
}

void Button::removeActionOnPressIndex(int index)
{
	actionsOnPress.erase(actionsOnPress.begin() + index);
}

void Button::pressButton()
{
	for (const std::function<void()> &function: actionsOnPress)
	{
		function();
	}
}

void Button::renderButton(sf::RenderWindow *window)
{
	window->draw(*this->m_sprite);
	window->draw(*this->m_text);
}

void Button::SetIdleColor(const sf::Color& color)
{
	m_idleColor = color;
	m_sprite->setColor(color);
}

float Button::GetXPosition()
{
	return this->m_sprite->getPosition().x;
}

float Button::GetYPosition()
{
	return this->m_sprite->getPosition().y;
}

float Button::GetWidth()
{
	return this->m_sprite->getScale().x;
}

float Button::GetHeight()
{
	return this->m_sprite->getScale().y;
}

int Button::GetCharacterSize()
{
	return this->m_text->getCharacterSize();
}

sf::Color Button::GetColor()
{
	return this->m_sprite->getColor();
}

#include "MainMenuScreen.h"
#include "Game.h"
#include "JoiningScreen.h"
#include "OptionsScreen.h"
#include "Client.h"
#include "SFML/Network.hpp"
#include "ExitScreen.h"
#include "GhostScreen.h"
#include "Log.h"

MainMenuScreen::MainMenuScreen()
{
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;
	
	auto widthButton = width / 6.6f;
	auto heightButton = height / 10.8f;
	
	// gap between buttons
	auto gap = height / 21.6f;
	
	auto characterSize = height / 27;
	
	Game::instance->getGraphicHandler()->SetTexture("background", "Resources/Textures/Backgrounds/MainMenu.jpg");
	Game::instance->getGraphicHandler()->SetTexture("logo", "Resources/Textures/logo.png");
	Game::instance->getGraphicHandler()->SetTexture("logoPL", "Resources/Textures/logoPL.png");
	
	sf::Font fontMagnificent;
	fontMagnificent.loadFromFile("Resources/Fonts/Magnificent.ttf");
	
	auto *host = new Button(width / 2.0f - widthButton / 2, height / 2.0f - heightButton - gap, widthButton,
	                        heightButton,
	                        &fontMagnificent, characterSize, "HOST",
	                        sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> HostButtonFunction = []()
	{
		Log::logger.GameStarted(__LINE__);
		Game::instance->InitServer();
		Game::instance->InitClient();
		Game::instance->GetClient()->StartConnectThread(sf::IpAddress::getLocalAddress());
		
	};
	host->addActionOnPress(HostButtonFunction);
	buttons.insert(host);
	
	
	auto *join = new Button(width / 2.0f - widthButton / 2, height / 2.0f, widthButton, heightButton,
	                        &fontMagnificent, characterSize, "JOIN",
	                        sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> JoinButtonFunction = []()
	{
		Game::instance->getGraphicHandler()->setTargetScreen(new JoiningScreen());
		Game::instance->InitClient();
	};
	join->addActionOnPress(JoinButtonFunction);
	buttons.insert(join);
	
	
	auto *options = new Button(width / 2.0f - widthButton / 2, height / 2.0f + heightButton + gap, widthButton,
	                           heightButton,
	                           &fontMagnificent, characterSize, "OPTIONS",
	                           sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> OptionsButtonFunction = []()
	{
		Game::instance->getGraphicHandler()->setTargetScreen(new OptionsScreen());
	};
	options->addActionOnPress(OptionsButtonFunction);
	buttons.insert(options);
	
	
	auto *quit = new Button(width / 2.0f - widthButton / 2, height - heightButton - gap, widthButton, heightButton,
	                        &fontMagnificent, characterSize, "QUIT",
	                        sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> QuitButtonFunction = []()
	{
		Game::instance->getGraphicHandler()->setTargetScreen(new ExitScreen());
	};
	
	quit->addActionOnPress((QuitButtonFunction));
	buttons.insert(quit);
	
}

MainMenuScreen::~MainMenuScreen()
= default;

void MainMenuScreen::updateFrame()
{
}

void MainMenuScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);
}


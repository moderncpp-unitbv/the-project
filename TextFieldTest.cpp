#include "gtest/gtest.h"
#include "SFML/include/SFML/Graphics.hpp"
#include "TextField.h"

class TextFieldTest : public ::testing::Test
{
protected:
    void SetUp() override
    {}

    void TearDown() override
    {}
};

TEST_F(TextFieldTest, TextFieldExist)
{
    sf::Font *font = new sf::Font();
    font->loadFromFile("Resources/Fonts/Magnificent.ttf");

    TextField *newTextField = new TextField(0, 0, 300, 200, font, 40, "newTextField", sf::Color::Magenta);

    EXPECT_GE(newTextField->GetXPosition(), 0);

    EXPECT_GE(newTextField->GetYPosition(), 0);

    EXPECT_GT(newTextField->GetWidth(), 0);

    EXPECT_GT(newTextField->GetHeight(), 0);

    EXPECT_GT(newTextField->GetTextSize(), 0);

    EXPECT_FALSE(newTextField->GetTextColor() == sf::Color::Transparent);

}
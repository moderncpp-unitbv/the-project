#include "BaseScreen.h"
#include "VisionCard.h"
#include "CharacterCard.h"
#include "Ghost.h"

class GhostScreen : public BaseScreen
{

public:
	
	GhostScreen();
	
	~GhostScreen();
	
	void updateFrame() override;
	
	void renderScreenFrame(sf::RenderWindow *window) override;
	
	void SelectVisionCard();

private:
	
	std::vector<VisionCard*> visionCards;
	std::vector<VisionCard*> m_selectedVisionCards;
	std::set<CardButton *> cardButtons;
	std::set<sf::Sprite*> m_tokens;
	Psychic *m_currentPsychic;
	Button *m_currentButton;
	
	static std::vector<TextField *> textFieldGuessing;

private:
	
	VisionCard* RollVisionCard(VisionCard*, bool);
	
	void PlaceVisionCards();

public:
	unsigned int GetVisionCardsSize();
	
	static TextField *getTextFieldGuessing(uint8_t index) ;
	
	static void setTextFieldGuessingText(int index, const std::string& newText);
	
	float GetWidth();

    float GetHeight();

};

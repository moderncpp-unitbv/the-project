#include "CardSet.h"

CardSet::CardSet(CharacterCard *character, LocationCard *location, ObjectCard *object)
		: m_character(character), m_location(location), m_object(object)
{}

CardSet::CardSet()
		: m_character(new CharacterCard(-1)), m_location(new LocationCard(-1)), m_object(new ObjectCard(-1))
{
	// empty card set
}

CharacterCard *CardSet::getCharacterCard() const
{
	return m_character;
}

LocationCard *CardSet::getLocationCard() const
{
	return m_location;
}

ObjectCard *CardSet::getObjectCard() const
{
	return m_object;
}

CardSet::~CardSet()
{
	delete m_character;
	delete m_location;
	delete m_object;
}



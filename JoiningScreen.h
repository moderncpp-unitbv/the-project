#ifndef MYSTERIUM_JOINEDMENUSCREEN_H
#define MYSTERIUM_JOINEDMENUSCREEN_H

#include "BaseScreen.h"
#include "Game.h"
#include "Client.h"

class JoiningScreen : public BaseScreen
{
public:
	JoiningScreen();

	~JoiningScreen();

	void updateFrame() override;

	void renderScreenFrame(sf::RenderWindow *window) override;

	unsigned int GetTextSize();

	sf::Color GetTextColor();

	const sf::Font *GetTextFont();

	static TextField *getRejectedMessage();

	static bool isShowRejectedMessage();

	static void setShowRejectedMessage(bool showRejectedMessage);

private:
	TextField *textField;
	sf::Font *font;

	static TextField *rejectedMessage;
	static bool showRejectedMessage;
};


#endif //MYSTERIUM_JOINEDMENUSCREEN_H

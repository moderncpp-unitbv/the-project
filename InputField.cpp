#include "InputField.h"
#include "Game.h"

InputField::InputField(float x, float y, float width, float height, sf::Font *font, int characterSize, uint16_t maxLength = 0)
                       : m_width(width), m_height(height), m_characterSize(characterSize)
{
	this->m_text = new sf::Text();
	this->m_sprite = new sf::Sprite();
	auto texture = new sf::Texture();
	
	this->m_font = font;
	this->m_maxLength = maxLength == 0 ? 2 * width / (float)characterSize : (float)maxLength;
	
	this->m_text->setFont(*m_font);
	this->m_text->setCharacterSize(characterSize);
	
	texture->loadFromFile("Resources/Textures/Buttons/inputField.png");
	m_sprite->setTexture(*texture);
	m_sprite->setColor(sf::Color(200, 200, 200));
	m_sprite->setPosition(x, y);
	sf::Vector2u textureSize = m_sprite->getTexture()->getSize();
	float scaleX = (float) width / textureSize.x;
	float scaleY = (float) height / textureSize.y;
	m_sprite->setScale(scaleX, scaleY);
	
	mousePressedFunction = [=](sf::Vector2i position, sf::Mouse::Button button)
	{
		return this->OnMousePressed(position, button);
	};
	
	Game::instance->addMousePressedFunction(&mousePressedFunction);
}

InputField::~InputField()
{
	delete this->m_sprite;
	delete this->m_text;
	Game::instance->removeMousePressedFunctionIndex(&this->mousePressedFunction);
}

bool InputField::OnMousePressed(sf::Vector2i position, sf::Mouse::Button button)
{
	if (button == sf::Mouse::Button::Left)
	{
		if (this->m_sprite->getGlobalBounds().contains(position.x, position.y - 25))
		{
			Game::instance->getInputHandler()->SelectInputField(this);
			return true;
		}
	}
	return false;
}

void InputField::UpdateText(const std::string &newText)
{
	std::string currentString = this->m_text->getString();
	
	if (currentString.length() + newText.length() > this->m_maxLength)
	{
		return;
	}
	
	this->m_text->setString(currentString + newText);
	CenterText();
}

void InputField::DeleteLastCharacter()
{
	auto s = this->m_text->getString().toAnsiString();
	
	if (s.empty())
		return;
	
	this->m_text->setString(s.substr(0, s.size() - 1));
	CenterText();
}

void InputField::Select()
{
	m_sprite->setColor(sf::Color(180, 180, 250));
}

void InputField::Deselect()
{
	m_sprite->setColor(sf::Color(100, 100, 100));
}

std::string InputField::GetInputText()
{
	return this->m_text->getString();
}

void InputField::renderInputField(sf::RenderWindow *window)
{
	window->draw(*this->m_sprite);
	window->draw(*this->m_text);
}

void InputField::ClearText()
{
	this->m_text->setString("");
}

void InputField::CenterText()
{
	sf::FloatRect textRect = m_text->getLocalBounds();
	m_text->setOrigin(textRect.left + textRect.width / 2.0f,
	                  textRect.top + textRect.height / 2.0f);
	m_text->setPosition(sf::Vector2f((float) m_sprite->getPosition().x + m_width / 2,
	                                 (float) m_sprite->getPosition().y + m_height / 2));
}

float InputField::GetXPosition()
{
    return this->m_sprite->getPosition().x;
}

float InputField::GetYPosition()
{
    return this->m_sprite->getPosition().y;
}

float InputField::GetWidth()
{
    return this->m_width;
}

float InputField::GetHeight()
{
    return this->m_height;
}

unsigned int InputField::GetCharacterSize()
{
    return this->m_characterSize;
}

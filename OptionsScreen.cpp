#include "SoundHandler.h"
#include "OptionsScreen.h"
#include "InputField.h"
#include "Game.h"

OptionsScreen::OptionsScreen()
{
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;
	
	// gap between buttons
	auto gap = height / 21.6f;
	
	auto x = width;
	auto y = height - 6 * gap;
	
	auto widthButton = width / 6.6f;
	auto heightButton = height / 10.8f;
	
	auto characterSize = height / 27;
	
	auto* font = new sf::Font;
	font->loadFromFile("Resources/Fonts/Magnificent.ttf");

    auto *name = new TextField((width - widthButton) / 2.0f , height / 2.0f - heightButton - 2 * gap,
                               widthButton, heightButton, font, characterSize, "BE SURE TO CHOOSE YOUR NAME",
                               sf::Color(165, 214, 244));
	if (Game::instance->GetPlayer()->GetName() != "Default")
		name->SetText("YOUR NAME IS " + Game::instance->GetPlayer()->GetName(), x, y);
	textFields.insert(name);
	
	auto *enterName = new Button((width - widthButton) / 2.4f, height / 2.f - heightButton,
	                             widthButton, heightButton, font, characterSize, "ENTER",
	                             sf::Color(150, 150, 150), sf::Color::White);
	
	auto *insertName = new InputField((width - widthButton) / 1.6f, height / 2.f - heightButton,
	                                  widthButton, heightButton, font, characterSize, 10);
	inputFields.insert(insertName);
	
	std::function<void()> EnterNameButtonFunction = [insertName, name, x, y]()
	{
	    if(insertName->GetInputText().empty())
        {
	        return;
        }

		Game::instance->GetPlayer()->SetName(insertName->GetInputText());
		name->SetText("YOUR NAME IS " + Game::instance->GetPlayer()->GetName(), x, y);
		name->renderTextField(Game::instance->getGraphicHandler()->window);
	};
	enterName->addActionOnPress(EnterNameButtonFunction);
	buttons.insert(enterName);
	
	
	auto *soundButton = new Button((width - widthButton) / 2.f, height / 2.f - heightButton + 3 * gap, widthButton, heightButton,
	                               font, characterSize, "SOUND",
	                               sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> SoundButtonFunction = []()
	{
		SoundHandler::instance->toggleMusic();
	};
	soundButton->addActionOnPress(SoundButtonFunction);
	buttons.insert(soundButton);
	
	
	auto *fullscreenButton = new Button((width - widthButton) / 2.4f, height / 2.f - heightButton + 6 * gap, widthButton, heightButton,
	                                    font, characterSize, "FULLSCREEN",
	                                    sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> FullscreenButtonFunction = [width,height]()mutable
	{
		Game::instance->getGraphicHandler()->window->setSize(sf::Vector2u(1920, 1080));
	};
	fullscreenButton->addActionOnPress(FullscreenButtonFunction);
	buttons.insert(fullscreenButton);
	
	
	auto *windowedButton = new Button((width - widthButton) / 1.6f, height / 2.f - heightButton + 6 * gap, widthButton, heightButton,
	                                  font, characterSize, "WINDOWED",
	                                  sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> WindowedButtonFunction = []()mutable
	{
		Game::instance->getGraphicHandler()->window->setSize(sf::Vector2u(1366, 768));
	};
	windowedButton->addActionOnPress(WindowedButtonFunction);
	buttons.insert(windowedButton);
	
	
	auto *backButton = new Button(width - widthButton - 20.f, height - heightButton - 20.f, widthButton, heightButton,
	                              font, characterSize, "BACK",
	                              sf::Color(150, 150, 150), sf::Color::White);
	std::function<void()> BackButtonFunction = []()
	{
		Game::instance->getGraphicHandler()->setTargetScreen(new MainMenuScreen());
	};
	backButton->addActionOnPress((BackButtonFunction));
	buttons.insert(backButton);
}

OptionsScreen::~OptionsScreen()
= default;

void OptionsScreen::updateFrame()
{
}

void OptionsScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);
}



#include "Card.h"
#include "Game.h"
#include <regex>
#include <typeindex>

template<typename Base, typename T>
inline bool instanceof(const T *ptr) {
	return dynamic_cast<const Base*>(ptr) != nullptr;
}

Card::Card(const std::string& path)
{
	std::regex reg(R"((Resources\/Textures\/[clo]cards\/[clo]c65535.jpg))");
	if (std::regex_search(path, reg))
	{
		// this is for the empty card set
	}
	else
	{
		try
		{
			m_sprite = new sf::Sprite();
		    m_texture = new sf::Texture();
			m_texture->loadFromFile(path);
		}
		catch (const std::string& errorMessage)
		{
			std::cout << errorMessage;
			return;
		}
		
		m_sprite->setTexture(*m_texture);

		m_highlight = new sf::RectangleShape();
		m_highlight->setSize(sf::Vector2f(Card::Width, Card::Height));
		m_highlight->setOutlineThickness(3);
		m_highlight->setOutlineColor(sf::Color::White);
	}
}

sf::RectangleShape* Card::GetHighlight() const
{
	return m_highlight;
}

sf::Texture* Card::GetTexture()
{
    return this->m_texture;
}

void Card::SetPosition(const float &x, const float &y)
{
	this->m_sprite->setPosition(x, y);
	this->m_highlight->setPosition(x, y);
}

sf::Sprite *Card::GetSprite()
{
	return this->m_sprite;
}

void Card::ResizeTexture()
{
	if (instanceof<CharacterCard>(this) || instanceof<LocationCard>(this))
	{
		this->m_sprite->setTextureRect(sf::IntRect(10, 10, 480, 310));
		this->m_highlight->setSize(sf::Vector2f(this->m_sprite->getTextureRect().width, this->m_sprite->getTextureRect().height));
		this->m_sprite->setScale(1, 1);
	}
	else if (instanceof<VisionCard>(this))
	{
		this->m_sprite->setTextureRect(sf::IntRect(10, 10, 315, 475));
		this->m_sprite->setScale(0.45f, 0.45f);
	}
	else
	{
		this->m_sprite->setTextureRect(sf::IntRect(15, 15, 295, 470));
		this->m_sprite->setScale(0.7f, 0.7f);
	}
}

Card::~Card()
= default;
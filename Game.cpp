#include "SFML/Graphics.hpp"
#include "Game.h"
#include "Server.h"
#include "LobbyScreen.h"
#include "Log.h"

template<class T>
void Game::RemoveFromVector(std::vector<T> &vector, const T &value)
{
	vector.erase(std::remove(vector.begin(), vector.end(), value), vector.end());
}

Game::Game()
{
	Log::logger.GameStarted(__LINE__);
	if (instance != nullptr)
	{
		delete instance;
	}
	instance = this;
	
	m_characterDeck = std::vector<CharacterCard *>();
	m_locationDeck = std::vector<LocationCard *>();
	m_objectDeck = std::vector<ObjectCard *>();
	
	int index;
	for (index = 0; index < MAX_CHARACTER_CARDS; index++)
	{
		auto temp = new CharacterCard(index);
		m_characterDeck.push_back(temp);
	}
	
	for (index = 0; index < MAX_LOCATION_CARDS; index++)
	{
		auto temp = new LocationCard(index);
		m_locationDeck.push_back(temp);
	}
	
	for (index = 0; index < MAX_OBJECT_CARDS; index++)
	{
		auto temp = new ObjectCard(index);
		m_objectDeck.push_back(temp);
	}
	
	for (index = 0; index < MAX_VISION_CARDS; index++)
	{
		auto temp = new VisionCard(index);
		m_visionDeck.push_back(temp);
	}
	
	m_player = new Player("Default");
	m_clock = new Clock;
	
	onMouseMovedFunctions = std::vector<std::function<bool(sf::Vector2i)> *>();
	onMousePressedFunctions = std::vector<std::function<bool(sf::Vector2i, sf::Mouse::Button)> *>();
	
	m_server = nullptr;
	m_client = nullptr;
	
	timeHandler = new TimeHandler();
	inputHandler = new InputHandler();
	graphicHandler = new GraphicHandler();
	graphicHandler->init();
}

std::vector<CharacterCard *> Game::GetCharacterDeck() const
{
	return m_characterDeck;
}

std::vector<LocationCard *> Game::GetLocationDeck() const
{
	return m_locationDeck;
}

std::vector<ObjectCard *> Game::GetObjectDeck() const
{
	return m_objectDeck;
}

std::vector<VisionCard *> Game::GetVisionDeck() const
{
	return m_visionDeck;
}

GraphicHandler *Game::getGraphicHandler() const
{
	return graphicHandler;
}

TimeHandler *Game::getTimeHandler() const
{
	return timeHandler;
}

InputHandler *Game::getInputHandler() const
{
	return this->inputHandler;
}

void Game::RemoveIndexFromCharacterDeck(int index)
{
	m_characterDeck.erase(m_characterDeck.begin() + index);
}

void Game::RemoveIndexFromLocationDeck(int index)
{
	m_locationDeck.erase(m_locationDeck.begin() + index);
}

void Game::RemoveIndexFromObjectDeck(int index)
{
	m_objectDeck.erase(m_objectDeck.begin() + index);
}

void Game::RemoveIndexFromVisionDeck(int index)
{
	m_visionDeck.erase(m_visionDeck.begin() + index);
}

void Game::addMouseMovedFunction(std::function<bool(sf::Vector2i)> *pFunction)
{
	onMouseMovedFunctions.push_back(pFunction);
}

void Game::addMousePressedFunction(std::function<bool(sf::Vector2i, sf::Mouse::Button)> *pFunction)
{
	onMousePressedFunctions.push_back(pFunction);
}

void Game::removeMouseMovedFunctionIndex(std::function<bool(sf::Vector2i)> *pFunction)
{
	RemoveFromVector(onMouseMovedFunctions, pFunction);
}

void Game::removeMousePressedFunctionIndex(std::function<bool(sf::Vector2i, sf::Mouse::Button)> *pFunction)
{
	RemoveFromVector(onMousePressedFunctions, pFunction);
}

void Game::updateGameFrame()
{
	sf::Time timeFromLastFrame = timeHandler->resetLastFrameClock();
	timeHandler->updateTimers(timeFromLastFrame.asSeconds());
	
	sf::Event e{};
	while (graphicHandler->window->pollEvent(e))
	{
		sf::Vector2i mouse = sf::Mouse::getPosition();
		sf::Vector2f mouseGlobal = graphicHandler->window->mapPixelToCoords(mouse);
		if (e.type == sf::Event::MouseMoved)
		{
			for (const std::function<bool(sf::Vector2i)> *function: onMouseMovedFunctions)
			{
				if ((*function)(sf::Vector2i(mouseGlobal.x, mouseGlobal.y)))
					break;
			}
		}
		if (e.type == sf::Event::MouseButtonPressed)
		{
			for (const std::function<bool(sf::Vector2i, sf::Mouse::Button)> *function: onMousePressedFunctions)
			{
				if ((*function)(sf::Vector2i(mouseGlobal.x, mouseGlobal.y), e.mouseButton.button))
				{
					break;
				}
			}
		}
		if (e.type == sf::Event::Closed)
		{
			graphicHandler->window->close();
		}
		if (e.type == sf::Event::KeyPressed)
		{
			if (e.key.code == sf::Keyboard::Escape)
			{
				graphicHandler->window->close();
			}
			else if (e.key.code == sf::Keyboard::Backspace)
			{
				inputHandler->DeleteLastCharacter();
			}
		}
		if (e.type == sf::Event::TextEntered)
		{
			if (e.text.unicode != '\b')
			{
				inputHandler->OnInputTextEntered(sf::String(e.text.unicode));
			}
		}
		if (e.type == sf::Event::Resized)
		{
			//getGraphicHandler()->actualScreen->UpdatePositions(getGraphicHandler()->window);
		}
	}
}

uint8_t Game::GetPlayersNumber()
{
	return getPlayersList().size();
}

std::vector<Player *> Game::getPlayersList()
{
	return PlayersList;
}

void Game::AddPlayer(Player *newPlayer)
{
	PlayersList.push_back(newPlayer);
}

void Game::RemovePlayerByName(const std::string &name)
{
	for (auto it = PlayersList.begin(); it != PlayersList.end(); it++)
	{
		if ((*it)->GetName() == name)
		{
			PlayersList.erase(it);
			return;
		}
	}
}

void Game::ClearPlayersList()
{
	PlayersList.clear();
}

void Game::SetMaxPlayers(uint8_t maxPlayers)
{
	this->MaxPlayers = maxPlayers;
}

uint8_t Game::GetMaxPlayers() const
{
	return this->MaxPlayers;
}

Player *Game::GetPlayer() const
{
	return m_player;
}

void Game::InitServer()
{
	Log::logger.ServerStart(__LINE__);
	m_server = new Server();
	m_server->StartServer();
}

void Game::CloseServer()
{
	Log::logger.ServerClosed(__LINE__);
	delete m_server;
}


void Game::InitClient()
{
	m_client = new Client();
}

Game::~Game()
{
	Log::logger.GameOver(__LINE__);
	for (auto mousePressedFunction: onMousePressedFunctions)
	{
		delete mousePressedFunction;
	}
	for (auto mouseMovedFunction: onMouseMovedFunctions)
	{
		delete mouseMovedFunction;
	}
	
	for (auto characterCard: m_characterDeck)
	{
		delete characterCard;
	}
	for (auto locationCard: m_locationDeck)
	{
		delete locationCard;
	}
	for (auto objectCard: m_objectDeck)
	{
		delete objectCard;
	}
	for (auto visionCard: m_visionDeck)
	{
		delete visionCard;
	}
	
	delete graphicHandler;
	delete timeHandler;
	delete inputHandler;
	
	if (IsServerActive())
	{
		delete m_server;
	}
	
	delete m_player;
}

Server *Game::GetServer() const
{
	return m_server;
}

bool Game::IsServerActive()
{
	if (m_server == nullptr)
	{
		return false;
	}
	
	return true;
}

Client *Game::GetClient() const
{
	return m_client;
}

void Game::RedrawPlayerList()
{
	if (auto *lobbyScreen = dynamic_cast<LobbyScreen *>(graphicHandler->actualScreen))
	{
		lobbyScreen->redrawPlayers();
	}
}

bool Game::IsCurrentPlayer(Player *other) const
{
	return m_player->GetName() == other->GetName();
}

void Game::AddToVisionDeck(VisionCard *visionCard)
{
	m_visionDeck.push_back(visionCard);
}

/*void Game::RunTests()
{
	testing::InitGoogleTest();
	RUN_ALL_TESTS();
}*/

Clock *Game::GetClock() const
{
	return m_clock;
}

std::vector<uint8_t> Game::GenerateCardSet()
{
	std::vector<uint8_t> vector;
	
	uint8_t characterCardIndex = rand() % (m_characterDeck.size() - 1);
	RemoveIndexFromCharacterDeck(characterCardIndex);
	vector.push_back(characterCardIndex);
	
	uint8_t locationCardIndex = rand() % (m_locationDeck.size() - 1);
	RemoveIndexFromLocationDeck(locationCardIndex);
	vector.push_back(locationCardIndex);
	
	uint8_t objectCardIndex = rand() % (m_objectDeck.size() - 1);
	RemoveIndexFromObjectDeck(objectCardIndex);
	vector.push_back(objectCardIndex);
	
	return vector;
}

#ifndef MYSTERIUMGAME_INPUTHANDLER_H
#define MYSTERIUMGAME_INPUTHANDLER_H

#include <string>
#include "InputField.h"

class InputHandler
{
public:
	InputHandler();

private:
	InputField *selectedInputField;

public:
	void OnInputTextEntered(const sf::String&);
	
	void SelectInputField(InputField *);
	
	void DeleteLastCharacter();
	
	void Reset();
};


#endif //MYSTERIUMGAME_INPUTHANDLER_H

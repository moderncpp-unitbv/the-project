#pragma once
#include "Card.h"
class LocationCard : public Card
{

public:

	LocationCard(const uint16_t&);
	bool operator==(const LocationCard&) const;

    uint16_t getNumber() const;

private:
	
	uint16_t m_number;
};


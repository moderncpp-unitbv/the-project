
#ifndef MYSTERIUM_OPTIONSSCREEN_H
#define MYSTERIUM_OPTIONSSCREEN_H

#include "BaseScreen.h"
#include "Button.h"
#include "Player.h"

class OptionsScreen:public BaseScreen
{
public:
	OptionsScreen();
	
	~OptionsScreen();
	
	void updateFrame() override;
	
	void renderScreenFrame(sf::RenderWindow *window) override;


private:


};


#endif //MYSTERIUM_OPTIONSSCREEN_H

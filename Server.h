#ifndef MYSTERIUMGAME_SERVER_H
#define MYSTERIUMGAME_SERVER_H

#include "SFML/Network.hpp"
#include "Game.h"
#include "Player.h"
#include "Client.h"
#include "ClientConnection.h"

class Server
{
public:
	Server();
	
	virtual ~Server();
	
	void StartServer();
	
	void SendStartGameCommand();
	
	void SendPlayerConnectedCommand(const Player &player, sf::TcpSocket *playerSocket);
	
	void SendPlayerDisconnectedCommand(const std::string &name);
	
	void SendPlayerListCommand(sf::TcpSocket *socket);
	
	void SendRejectedClientCommand(sf::TcpSocket *socket);
	
	void SendAdvanceCommand();
	
	bool CanStartGame();
	
	static bool isValidIPAddress(const std::string &ipAddress);
	
	static bool isNumber(const std::string &string);
	
	void SendRoleChangeCommand(sf::TcpSocket *socket, const std::string &playerName, sf::Int8 role);
	
	void SendVisionCardsCommand(sf::TcpSocket *psychicSocket, unsigned int size, const std::set<uint8_t> &cards);


private:
	void Listen();
	
	void ProcessPacket(sf::TcpSocket &clientSocket, sf::Packet packet);
	
	bool RemovePlayerWithName(const std::string &name);

private:
	sf::SocketSelector *socketSelector;
	sf::TcpListener *listener;
	
	std::set<ClientConnection *> connections;
	
	sf::Thread *serverListenThread;
	
	uint8_t guessedCount;
};


#endif //MYSTERIUMGAME_SERVER_H

#ifndef MYSTERIUMGAME_CLIENTCONNECTION_H
#define MYSTERIUMGAME_CLIENTCONNECTION_H


#include <SFML/Network/TcpSocket.hpp>
#include "Player.h"

class ClientConnection
{
public:
	ClientConnection();
	
	ClientConnection(sf::TcpSocket *mSocket);
	
	virtual ~ClientConnection();
	
	uint16_t getId() const;
	
	void setId(uint16_t id);
	
	sf::TcpSocket *getSocket() const;
	
	void setSocket(sf::TcpSocket *socket);
	
	Player *getPlayer() const;
	
	void setPlayer(Player *player);

    uint16_t getPsychicId() const;

    void setPsychicId(uint16_t mPsychicId);

private:
	uint16_t m_id;
	uint16_t m_psychicID;
	sf::TcpSocket *m_socket;
	Player *m_player;
	
	static uint16_t lastID;
};


#endif //MYSTERIUMGAME_CLIENTCONNECTION_H

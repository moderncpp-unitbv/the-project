#include "Game.h"

CardButton::CardButton(float x, float y, float width, float height, Card *card)
{
	idleColor = sf::Color(150, 100, 0);
	activeColor = sf::Color(0, 50, 200);
	hoverColor = sf::Color(50, 150, 0);
	
	shape = new sf::RectangleShape();
	shape->setPosition(sf::Vector2f(x, y));
	shape->setSize(sf::Vector2(width, height));
	shape->setOutlineColor(idleColor);
	shape->setOutlineThickness(5);
	m_card = card;

	hovering = false;
	pressed = false;
	m_visible = true;
	
	mouseMovedFunction = [=](sf::Vector2i position)
	{
		return this->onMouseHover(position);
	};
	
	mousePressedFunction = [=](sf::Vector2i position, sf::Mouse::Button button)
	{
		return this->onMousePressed(position, button);
	};
	
	Game::instance->addMouseMovedFunction(&mouseMovedFunction);
	Game::instance->addMousePressedFunction(&mousePressedFunction);
}

CardButton::CardButton(float width, float height, Card *card)
{
	idleColor = sf::Color(150, 100, 0);
	activeColor = sf::Color(0, 50, 200);
	hoverColor = sf::Color(50, 150, 0);
	
	auto cardPosition = card->GetSprite()->getPosition();
	shape = new sf::RectangleShape();
	shape->setPosition(sf::Vector2f(cardPosition.x, cardPosition.y));
	shape->setSize(sf::Vector2(width, height));
	shape->setOutlineColor(idleColor);
	shape->setOutlineThickness(5);
	m_card = card;

	hovering = false;
	pressed = false;
	m_visible = true;
	
	mouseMovedFunction = [=](sf::Vector2i position)
	{
		return this->onMouseHover(position);
	};
	
	mousePressedFunction = [=](sf::Vector2i position, sf::Mouse::Button button)
	{
		return this->onMousePressed(position, button);
	};
	
	Game::instance->addMouseMovedFunction(&mouseMovedFunction);
	Game::instance->addMousePressedFunction(&mousePressedFunction);
}


CardButton::~CardButton()
{
	Game::instance->removeMouseMovedFunctionIndex(&mouseMovedFunction);
	Game::instance->removeMousePressedFunctionIndex(&mousePressedFunction);
	Game::instance->getTimeHandler()->removeTimer(&colorBackFunction);
}

bool CardButton::onMouseHover(sf::Vector2i position)
{
	if (m_visible)
		if (!pressed)
		{
			if (this->shape->getGlobalBounds().contains(position.x, position.y))
			{
				if (!hovering)
				{
					hovering = true;
					this->shape->setOutlineColor(hoverColor);
				}
			}
			else
			{
				if (hovering)
				{
					hovering = false;
					this->shape->setOutlineColor(idleColor);
				}
			}
		}
	return false;
}

bool CardButton::onMousePressed(sf::Vector2i position, sf::Mouse::Button button)
{
	if (m_visible)
		if (button == sf::Mouse::Button::Left)
		{
			if (this->shape->getGlobalBounds().contains(position.x, position.y))
			{
				if (!pressed)
				{
					pressed = true;
					hovering = false;
					this->shape->setOutlineColor(activeColor);
					
					colorBackFunction = [=]()
					{
						this->onMouseHover(sf::Mouse::getPosition());
					};
					Game::instance->getTimeHandler()->setTimer(&colorBackFunction, 0.1f);
					
					pressButton();
					
					return true;
				}
				else
				{
					pressed = false;
				}
			}
		}
	return false;
}

void CardButton::addActionOnPress(const std::function<void()> &pFunction)
{
	actionsOnPress.push_back(pFunction);
}

void CardButton::removeActionOnPressIndex(int index)
{
	actionsOnPress.erase(actionsOnPress.begin() + index);
}

void CardButton::pressButton()
{
	for (const std::function<void()> &function: actionsOnPress)
	{
		function();
	}
}

void CardButton::renderButton(sf::RenderWindow *window)
{
	window->draw(*this->shape);
}

bool CardButton::IsPressed() const
{
	return pressed;
}

Card *CardButton::GetCard() const
{
	return m_card;
}

void CardButton::Reset()
{
	pressed = false;
	shape->setOutlineColor(idleColor);
}

void CardButton::SetCard(Card *other)
{
	m_card = other;
}

bool CardButton::IsVisible() const
{
	return m_visible;
}

void CardButton::SetVisible(bool visible)
{
	m_visible = visible;
}

#pragma once
#include <ostream>

#define LOGGING_EXPORTS

#ifdef LOGGING_EXPORTS
#define LOGGING_API __declspec(dllexport)
#else
#define LOGGING_API __declspec(dllimport)
#endif


class LOGGING_API Logging
{
public:
	enum class Level {
		Info,
		Warning,
		Error
	};

public:
	Logging(std::ostream& flux, Level minimumLevel = Level::Info);

	void Log(std::string& message, Level level);



private:
	Level m_minimumLevel;
	std::ostream& m_flux;



};
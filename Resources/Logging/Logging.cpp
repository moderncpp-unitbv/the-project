#include "pch.h"
#include "Logging.h"

Logging::Logging(std::ostream& flux, Level minimumLevel)
	:m_flux(flux), m_minimumLevel(minimumLevel)
{

}

void Logging::Log(std::string& message, Level level)
{
	if (level >= m_minimumLevel) {
		m_flux << message << std::endl;
	}
}

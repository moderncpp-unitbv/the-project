#include "TextField.h"

TextField::TextField(float x, float y, float width, float height, sf::Font *font, int characterSize, const std::string &text,
               sf::Color color)
{
	this->m_shape = new sf::RectangleShape();
	this->m_text = new sf::Text();
	this->m_texture = new sf::Texture;
	
	this->m_texture->loadFromFile("Resources/Textures/Buttons/button.png");
	
	this->m_shape->setPosition(sf::Vector2f(x, y));
	this->m_shape->setSize(sf::Vector2(width, height));
	this->m_shape->setFillColor(color);
	this->m_shape->setTexture(m_texture);
	
	this->m_font = *font;
	this->m_text->setFont(this->m_font);
	this->m_text->setString(text);
	this->m_text->setFillColor(sf::Color::Black);
	this->m_text->setOutlineColor(color);
	this->m_text->setOutlineThickness(2);
	this->m_text->setCharacterSize(characterSize);

	this->m_text->setPosition(this->m_shape->getPosition().x + this->m_shape->getGlobalBounds().width / 2.0f -
	                        this->m_text->getGlobalBounds().width / 2.0f,
	                        this->m_shape->getPosition().y + this->m_shape->getGlobalBounds().height / 2.0f -
	                        this->m_text->getGlobalBounds().height);
	
	this->m_characterSize = characterSize;
}

void TextField::renderTextField(sf::RenderWindow *window)
{
	window->draw(*this->m_text);
}

void TextField::SetText(const std::string &text, const unsigned int &width, const unsigned int &height)
{
	m_text->setString(text);
	sf::FloatRect textRect = m_text->getLocalBounds();
	m_text->setOrigin(textRect.left + textRect.width / 2.0f,
	                textRect.top + textRect.height / 2.0f);
	m_text->setPosition(sf::Vector2f((float) width / 2, (float) height / 2));
}

void TextField::EditText(const std::string &text)
{
	m_text->setString(text);
}

void TextField::SetColor(const sf::Color &color)
{
	this->m_text->setOutlineColor(color);
}

unsigned int TextField::GetWidth()
{
    return this->m_shape->getSize().x;
}

unsigned int  TextField::GetHeight()
{
    return this->m_shape->getSize().y;
}

unsigned int TextField::GetTextSize()
{
    return this->m_text->getCharacterSize();
}

sf::Color TextField::GetTextColor()
{
    return this->m_text->getFillColor();
}

float TextField::GetXPosition()
{
    return this->m_shape->getPosition().x;
}

float TextField::GetYPosition()
{
    return this->m_shape->getPosition().y;
}

void TextField::CenterText()
{
    sf::FloatRect textRect = m_text->getLocalBounds();
    m_text->setOrigin(textRect.left + textRect.width / 2.0f,textRect.top + textRect.height / 2.0f);
}

#pragma once
#include <iostream>
#include <SFML\Graphics.hpp>
class Card 
{

public:

	static const int Width = 500;
	static const int Height = 300;
	
	Card(const std::string&);
	virtual ~Card();

	sf::RectangleShape* GetHighlight() const;
	void MoveShape(std::pair<double, double>);
	void ResizeShape(std::pair<double, double>);
	void SetPosition(const float&, const float&);
	void ResizeTexture();

	sf::Texture* GetTexture();
	sf::Sprite* GetSprite();

private:

	sf::Texture* m_texture;
	sf::Sprite* m_sprite;
	sf::RectangleShape* m_highlight;
};
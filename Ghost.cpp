#include <random>
#include <chrono>
#include <utility>
#include "Ghost.h"
#include "Log.h"


Ghost::Ghost()
{
	Log::logger.GhostJoined(__LINE__);
	m_psychics = std::vector<Psychic *>();
	m_visions = std::vector<VisionCard *>();
	m_crows = 0;
}

VisionCard *Ghost::GetVision(const int &index)
{
	return m_visions.at(index);
}

void Ghost::CreatePsychic(CardSet *clues, uint8_t index)
{
	Log::logger.PsychicJoined(__LINE__);
	auto newPsychic = new Psychic(clues, index);
	m_psychics.push_back(newPsychic);
}

std::vector<Psychic *> Ghost::getPsychics()
{
	return m_psychics;
}

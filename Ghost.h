#pragma once

#include "PlayerRole.h"
#include "CardSet.h"
#include "VisionCard.h"
#include "Game.h"
#include "Psychic.h"

class Ghost
{

public:
	Ghost();
	
	VisionCard *GetVision(const int &);
	
	static void GiveVisions(Psychic &, const std::vector<VisionCard *> &);
	
	void ChangeVision(std::vector<VisionCard>, int);
	
	void CreatePsychic(CardSet *clues, uint8_t index);
	
	std::vector<Psychic *> getPsychics();

private:
	std::vector<Psychic *> m_psychics;
	std::vector<VisionCard *> m_visions;
	
	uint8_t m_crows;
};


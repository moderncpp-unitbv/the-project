#pragma once

#include <iostream>
#include "SFML/Network.hpp"
#include "PlayerRole.h"
#include "ChoosingType.h"
#include "Psychic.h"
#include "Ghost.h"

class Ghost;

class Player
{
private:
	std::string m_name;
	PlayerRole m_role;
	ChoosingType m_choosingType;
	
	Ghost *m_ghost;
	
	Psychic *m_psychic;

public:
	Player();
	
	Player(std::string name);
	
	virtual ~Player();
	
	void SetRole(PlayerRole targetRole);
	
	void SetName(std::string targetName);
	
	std::string GetName() const;
	
	PlayerRole GetRole() const;
	
	std::string GetRoleName() const;
	
	ChoosingType GetChoosingType() const;
	
	std::string GetChoosingTypeName() const;
	
	void AdvanceChoosing();
	
	Ghost *getGhost() const;
	
	void setGhost(Ghost *mGhost);
	
	Ghost *InitializeGhost();
	
	Psychic *getPsychic() const;
	
	void setPsychic(Psychic *mPsychic);
	
	Psychic *InitializePsychic(CardSet *clue, uint8_t psychicIndex);
	
	friend sf::Packet &operator<<(sf::Packet &packet, const Player &player);
	
	friend sf::Packet &operator>>(sf::Packet &packet, Player &player);
};

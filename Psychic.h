#pragma once

#include "PlayerRole.h"
#include "VisionCard.h"
#include "CardSet.h"
#include <vector>

class Psychic
{
public:
	Psychic(std::string name);
	
	Psychic();
	
	Psychic(CardSet*, const uint8_t&);
	
	CardSet* GetCardSet() const;
	
	sf::Sprite* GetGhostToken() const;

	uint8_t GetIndex() const;
	
	void SetCardSet(CardSet *cluesSet);

    void SetVisions(std::vector<VisionCard *>);

    std::vector<VisionCard *> GetVisions() const;
	
	bool isGuessed() const;
	
	void setGuessed(bool guessed);
	
	void ResetVisions();

    void ReceiveVisions(std::vector<VisionCard *> visions);
    
    bool HasReceivedVisions() const;

    uint8_t getPoints() const;

    void IncreasePoints();


private:
	uint8_t m_level = 1;
	std::string m_name;
	uint8_t m_points=0;
    std::vector<VisionCard *> m_visions;
    CardSet *m_cluesSet;
    sf::Sprite *m_ghostToken;
    uint8_t m_indexInGame;
    bool guessed = false;
};


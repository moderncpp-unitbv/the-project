#include <gtest/gtest.h>
#include "SFML/include/SFML/Graphics.hpp"
#include "LobbyScreen.h"

class LobbyScreenTest : public ::testing::Test
{
protected:
    void SetUp() override
    {}

    void TearDown() override
    {}
};

TEST_F(LobbyScreenTest, TextExist)
{
    LobbyScreen *newScreen = new LobbyScreen();

    EXPECT_TRUE(newScreen->GetFont());

    EXPECT_GT(newScreen->GetPlayersCharacterSize(), 0);

    EXPECT_FALSE(newScreen->GetPlayersColor() == sf::Color::Transparent);

    EXPECT_GT(newScreen->GetTextWidth(), 0);

    EXPECT_GT(newScreen->GetTextHeight(), 0);

}
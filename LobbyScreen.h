#ifndef MYSTERIUMGAME_LOBBYSCREEN_H
#define MYSTERIUMGAME_LOBBYSCREEN_H

#include <iostream>
#include <set>
#include <ntdef.h>
#include "SFML/Graphics.hpp"
#include "BaseScreen.h"
#include "Game.h"
#include "Button.h"
#include "Player.h"
#include "Server.h"

class LobbyScreen : public BaseScreen
{
private:
	static sf::Font *fontMagnificent;
	TextField *cod;
	TextField *players;
	
	Player *m_currentPlayer;
	
	static std::vector<TextField *> texts;

public:
	LobbyScreen();
	
	~LobbyScreen();
	
	void updateFrame() override;
	
	void renderScreenFrame(sf::RenderWindow *window) override;
	
	void static redrawPlayers();

	sf::Font * GetFont();

	unsigned int GetPlayersCharacterSize();

	sf::Color GetPlayersColor();

	float GetTextWidth();

	float GetTextHeight();
};


#endif //MYSTERIUMGAME_LOBBYSCREEN_H

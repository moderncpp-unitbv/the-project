#include <gtest/gtest.h>
#include "PsychicScreen.h"

class PsychicScreenTest : public ::testing::Test
{
protected:
    void SetUp() override
    {}

    void TearDown() override
    {}
};

TEST_F(PsychicScreenTest, IsCreated)
{
    auto *newScreen = new PsychicScreen();
	
//	EXPECT_LT(0, newScreen->GetVisionCardsSize());
	
//	EXPECT_LT(0, newScreen->GetCardSetsSize());
	
	EXPECT_LT(0, newScreen->GetWidth());
	
	EXPECT_LT(0, newScreen->GetHeight());
	
	delete newScreen;
}
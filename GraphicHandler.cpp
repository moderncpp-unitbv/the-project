#include "GraphicHandler.h"
#include "GhostScreen.h"
#include "PsychicScreen.h"
#include "LobbyScreen.h"

template<typename Base, typename T>
inline bool instanceof(const T *ptr)
{
	return dynamic_cast<const Base *>(ptr) != nullptr;
}

GraphicHandler::~GraphicHandler()
{
	delete window;
}

void GraphicHandler::init()
{
	initWindow();
	Game::instance->RunTests();
	setTargetScreen(new MainMenuScreen());
	
	while (window->isOpen())
	{
		changeScreen();
		
		Game::instance->updateGameFrame();
		
		actualScreen->updateFrame();
		
		this->renderGraphicFrame();
	}
}

void GraphicHandler::initWindow()
{
	this->window = new sf::RenderWindow(sf::VideoMode::getDesktopMode(), "Mysterium",
	                                    sf::Style::Close);
}

void GraphicHandler::setTargetScreen(BaseScreen *screen)
{
	targetScreen = screen;
}

void GraphicHandler::changeScreen()
{
	if (targetScreen == nullptr)
		return;
	
	delete this->actualScreen;
	this->actualScreen = targetScreen;
	targetScreen = nullptr;
	Game::instance->getInputHandler()->Reset();
}

void GraphicHandler::renderGraphicFrame() const
{
	// draw everything manually
	this->window->draw(*sprites.at("background"));
	if (!instanceof<GhostScreen>(actualScreen) && !instanceof<PsychicScreen>(actualScreen))
	{
		if (!instanceof<LobbyScreen>(actualScreen))
			this->window->draw(*sprites.at("logo"));
		else
			this->window->draw(*sprites.at("logoPL"));
	}
	
	this->actualScreen->renderScreenFrame(this->window);
	
	this->window->display();
}

void GraphicHandler::SetTexture(const std::string &key, const std::string &path)
{
	auto *texture = new sf::Texture();
	if (!texture->loadFromFile(path))
	{
		std::cout << "Couldn't access " + path;
		return;
	}
	
	this->textures.at(key) = texture;
	auto temp = new sf::Sprite(*textures.at(key));
	
	sf::Vector2u textureSize = temp->getTexture()->getSize();
	
	if (key == "background")
	{
		float scaleX = (float) 1920 / textureSize.x;
		float scaleY = (float) 1080 / textureSize.y;
		temp->setScale(scaleX, scaleY);
	}
		//0.8928
	else if (key == "logo")
	{
		auto width = 581;
		auto height = 200;
		
		float scaleX = (float) width / textureSize.x;
		float scaleY = (float) height / textureSize.y;
		temp->setScale(scaleX, scaleY);
		temp->setPosition(1920 / 2.0f - width / 2, 100);
	}
	else if (key == "logoPL")
	{
		auto width = 581;
		auto height = 867.8;
		
		if (key == "background")
		{
			float scaleX = (float) 1920 / textureSize.x;
			float scaleY = (float) 1080 / textureSize.y;
			temp->setScale(scaleX, scaleY);
		}
		else if (key == "logo")
		{
			auto width = 581;
			auto height = 200;
			
			float scaleX = (float) width / textureSize.x;
			float scaleY = (float) height / textureSize.y;
			temp->setScale(scaleX, scaleY);
		}
		else if (key == "logoPL")
		{
			auto width = 581;
			auto height = 867.8;
			
			float scaleX = (float) width / textureSize.x;
			float scaleY = (float) height / textureSize.y;
			temp->setScale(scaleX, scaleY);
		}
		temp->setPosition(1920 / 2.0f - width / 2, 100);
	}
	
	this->sprites[key] = temp;
	
}
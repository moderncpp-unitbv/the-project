#include "PsychicScreen.h"
#include "Client.h"
#include "Log.h"

PsychicScreen::PsychicScreen()
{
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;
	
	auto widthButton = width / 6.6f;
	auto heightButton = height / 10.8f;
	
	// gap between buttons
	auto gap = height / 21.6f;
	auto characterSize = height / 27;
	
	auto clock = Game::instance->GetClock();
	
	sf::Font fontMagnificent;
	fontMagnificent.loadFromFile("Resources/Fonts/Magnificent.ttf");
	
	m_statusText = new TextField(250, 30, 300, 40, &fontMagnificent, 40,
	                             "", sf::Color::White);
	Game::instance->getGraphicHandler()->SetTexture("background", "Resources/Textures/Backgrounds/board.jpg");
	
	m_selectedCharacterCard = nullptr;
	m_selectedLocationCard = nullptr;
	m_selectedObjectCard = nullptr;
	
	m_pointsText = new TextField(320, 105, 300, 40, &fontMagnificent, 40,
	                             "POINTS:", sf::Color::White);
	
	m_visionsText = new TextField(width - 500, 60, 300, 40, &fontMagnificent, 35,
	                              "YOU HAVEN'T RECEIVED ANY VISIONS YET", sf::Color::White);
	
	points = new TextField(390, 75, 300, 40, &fontMagnificent, 40,
	                       "", sf::Color::White);
	
	for (unsigned int index = 0; index < 4; index++)
	{
		
		auto characterCard = Game::instance->GetCharacterDeck().at(index);
		auto locationCard = Game::instance->GetLocationDeck().at(index);
		auto objectCard = Game::instance->GetObjectDeck().at(index);
		
		characterCard->ResizeTexture();
		locationCard->ResizeTexture();
		objectCard->ResizeTexture();
		
		if (index % 2 == 0)
		{
			characterCard->SetPosition(150.0f + index * 310.0f, 280.0f);
			locationCard->SetPosition(150.0f + index * 310.0f, 280.0f);
			objectCard->SetPosition(150.0f + index * 310.0f, 280.0f);
		}
		else
		{
			characterCard->SetPosition(150.0f + (index - 1) * 310.0f, 630.0f);
			locationCard->SetPosition(150.0f + (index - 1) * 310.0f, 630.0f);
			objectCard->SetPosition(150.0f + (index - 1) * 310.0f, 630.0f);
		}
		
		auto characterButton = new CardButton(480, 310, characterCard);
		std::function<void()> characterButtonFunction = [this, characterButton]()
		{
			for (auto button : characterButtons)
			{
				if (button != characterButton)
					if (button->IsPressed())
					{
						button->Reset();
						m_selectedCharacterCard = nullptr;
					}
			}
			this->m_selectedCharacterCard = dynamic_cast<CharacterCard *>(characterButton->GetCard());
		};
		characterButton->addActionOnPress(characterButtonFunction);
		characterButtons.insert(characterButton);
		
		auto locationButton = new CardButton(480, 310, locationCard);
		std::function<void()> locationButtonFunction = [this, locationButton]()
		{
			for (auto button : locationButtons)
			{
				if (button != locationButton)
					if (button->IsPressed())
					{
						button->Reset();
						m_selectedLocationCard = nullptr;
					}
			}
			if (locationButton->IsPressed())
				this->m_selectedLocationCard = dynamic_cast<LocationCard *>(locationButton->GetCard());
			else
				this->m_selectedLocationCard = nullptr;
		};
		locationButton->addActionOnPress(locationButtonFunction);
		locationButtons.insert(locationButton);
		
		auto objectButton = new CardButton(206, 329, objectCard);
		std::function<void()> objectButtonFunction = [this, objectButton]()
		{
			for (auto button : objectButtons)
			{
				if (button != objectButton)
					if (button->IsPressed())
					{
						button->Reset();
					}
			}
			if (objectButton->IsPressed())
				this->m_selectedObjectCard = dynamic_cast<ObjectCard *>(objectButton->GetCard());
			else
				this->m_selectedObjectCard = nullptr;
		};
		objectButton->addActionOnPress(objectButtonFunction);
		objectButtons.insert(objectButton);
		
		auto clues = new CardSet(characterCard, locationCard, objectCard);
		cardSets.push_back(clues);
		
		auto *makeGuess = new Button(width - widthButton - 20, height - heightButton - 20,
		                             widthButton, heightButton, &fontMagnificent, characterSize, "MAKE GUESS",
		                             sf::Color(150, 150, 150), sf::Color::White);
		std::function<void()> MakeGuessButtonFunction = [this]()
		{
			if (Game::instance->GetPlayer()->getPsychic()->isGuessed())
				return;
			
			auto choosingType = Game::instance->GetPlayer()->GetChoosingType();
			switch (choosingType)
			{
				case ChoosingType::CHARACTER:
				{
					if (!IsPressed(m_selectedCharacterCard))
					{
						Log::logger.PsychicChooseCard(__LINE__);
						break;
					}
					
					auto vector = Game::instance->GetCharacterDeck();
					Log::logger.PsychicChose(__LINE__);
					
					if (m_selectedCharacterCard ==
					    Game::instance->GetPlayer()->getPsychic()->GetCardSet()->getCharacterCard())
					{
						Log::logger.PsychicCharacterCorrect(__LINE__);
						Game::instance->GetClient()->SendAdvanceCommand(1);
						Game::instance->GetPlayer()->AdvanceChoosing();
						
						for (auto button : characterButtons)
						{
							button->SetVisible(false);
						}
						for (auto button : locationButtons)
						{
							button->SetVisible(true);
						}
					}
					else
					{
						Log::logger.PsychicCharacterWrong(__LINE__);
						Game::instance->GetClient()->SendAdvanceCommand(0);
					}
					break;
				}
				case ChoosingType::LOCATION:
				{
					if (!IsPressed(m_selectedLocationCard))
					{
						Log::logger.PsychicChooseCard(__LINE__);
						break;
					}
					
					auto vector = Game::instance->GetLocationDeck();
					Log::logger.PsychicChose(__LINE__);
					
					if (m_selectedLocationCard ==
					    Game::instance->GetPlayer()->getPsychic()->GetCardSet()->getLocationCard())
					{
						Log::logger.PsychicLocationCorrect(__LINE__);
						Game::instance->GetClient()->SendAdvanceCommand(1);
						Game::instance->GetPlayer()->AdvanceChoosing();
						
						for (auto button : locationButtons)
						{
							button->SetVisible(false);
						}
						for (auto button : objectButtons)
						{
							button->SetVisible(true);
						}
					}
					else
					{
						Log::logger.PsychicLocationWrong(__LINE__);
						Game::instance->GetClient()->SendAdvanceCommand(0);
					}
					break;
				}
				case ChoosingType::OBJECT:
				{
					if (!IsPressed(m_selectedObjectCard))
					{
						Log::logger.PsychicChooseCard(__LINE__);
						break;
					}
					
					auto vector = Game::instance->GetObjectDeck();
					Log::logger.PsychicChose(__LINE__);
					
					if (m_selectedObjectCard ==
					    Game::instance->GetPlayer()->getPsychic()->GetCardSet()->getObjectCard())
					{
						Log::logger.PsychicObjectCorrect(__LINE__);
						Game::instance->GetClient()->SendAdvanceCommand(1);
						Game::instance->GetPlayer()->AdvanceChoosing();
						
						for (auto button : objectButtons)
						{
							button->SetVisible(false);
						}
					}
					else
					{
						Log::logger.PsychicObjectWrong(__LINE__);
						Game::instance->GetClient()->SendAdvanceCommand(0);
					}
					break;
				}
				default:
					break;
			}
			
		};
		makeGuess->addActionOnPress(MakeGuessButtonFunction);
		buttons.insert(makeGuess);
		
		for (auto button : characterButtons)
		{
			button->SetVisible(true);
		}
		for (auto button : locationButtons)
		{
			button->SetVisible(false);
		}
		for (auto button : objectButtons)
		{
			button->SetVisible(false);
		}
	}
}

bool PsychicScreen::IsPressed(Card *card) const
{
	if (card == nullptr)
		return false;
	for (auto button : characterButtons)
		if (button->GetCard() == card)
			return button->IsPressed();
	for (auto button : locationButtons)
		if (button->GetCard() == card)
			return button->IsPressed();
	for (auto button : objectButtons)
		if (button->GetCard() == card)
			return button->IsPressed();
	return false;
}

PsychicScreen::~PsychicScreen()
= default;

void PsychicScreen::updateFrame()
{
}

void PsychicScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);
	auto choosingType = Game::instance->GetPlayer()->GetChoosingType();
	
	switch (choosingType)
	{
		case ChoosingType::CHARACTER:
			
			for (auto button : characterButtons)
			{
				button->renderButton(window);
			}
			for (auto clues : cardSets)
			{
				window->draw(*clues->getCharacterCard()->GetSprite());
			}
			m_statusText->EditText("YOU NEED TO CHOOSE A CHARACTER CARD");
			break;
		
		case ChoosingType::LOCATION:
			
			for (auto button : locationButtons)
			{
				button->renderButton(window);
			}
			for (auto clues : cardSets)
			{
				window->draw(*clues->getLocationCard()->GetSprite());
			}
			m_statusText->EditText("YOU NEED TO CHOOSE A LOCATION CARD");
			break;
		
		case ChoosingType::OBJECT:
			
			for (auto button : objectButtons)
			{
				button->renderButton(window);
			}
			for (auto clues : cardSets)
			{
				window->draw(*clues->getObjectCard()->GetSprite());
			}
			m_statusText->EditText("YOU NEED TO CHOOSE AN OBJECT CARD");
			break;
		
		case ChoosingType::FINAL:
			Log::logger.PsychicWon(__LINE__);
			break;
	}
	
	auto visions = Game::instance->GetPlayer()->getPsychic()->GetVisions();
	auto width = sf::VideoMode::getDesktopMode().width;
	if (visions.empty())
	{
		m_visionsText->EditText("YOU HAVEN'T RECEIVED ANY VISIONS YET");
	}
	else
	{
		m_visionsText->EditText("THESE ARE YOUR VISIONS");
		for (int index = 0; index < visions.size(); index++)
		{
			visions.at(index)->ResizeTexture();
			
			if (index % 2 == 0)
			{
				visions.at(index)->GetSprite()->setPosition(width - 355.0f, 150.0f + index * 120.0f);
			}
			if (index % 2 == 1)
			{
				visions.at(index)->GetSprite()->setPosition(width - 200.0f, 150.0f + (index - 1) * 120.0f);
			}
			
			window->draw(*visions.at(index)->GetSprite());
		}
	}
	
	
	Game::instance->GetClock()->RenderClock(window);
	m_statusText->renderTextField(window);
	
	m_pointsText->renderTextField(window);
	m_visionsText->renderTextField(window);
	
	points->EditText(std::to_string(m_psychic->getPoints()));
	points->renderTextField(window);
	
	m_statusText->renderTextField(window);
	
}

unsigned int PsychicScreen::GetVisionCardsSize()
{
	return this->m_visionCards.size();
}

unsigned int PsychicScreen::GetCardSetsSize()
{
	return this->cardSets.size();
}

float PsychicScreen::GetWidth()
{
	return Game::instance->getGraphicHandler()->window->getSize().x;
}

float PsychicScreen::GetHeight()
{
	return Game::instance->getGraphicHandler()->window->getSize().y;
}

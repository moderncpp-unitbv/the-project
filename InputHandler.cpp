#include "InputHandler.h"

InputHandler::InputHandler()
{
	this->selectedInputField = nullptr;
}

void InputHandler::SelectInputField(InputField *inputField)
{
	if (this->selectedInputField != nullptr)
	{
		this->selectedInputField->Deselect();
	}
	
	this->selectedInputField = inputField;
	this->selectedInputField->Select();
}

void InputHandler::OnInputTextEntered(const sf::String& text)
{
	if (this->selectedInputField == nullptr)
		return;
	
	this->selectedInputField->UpdateText(text.toAnsiString());
}

void InputHandler::DeleteLastCharacter()
{
	if (this->selectedInputField == nullptr)
		return;
	
	this->selectedInputField->DeleteLastCharacter();
}

void InputHandler::Reset()
{
	this->selectedInputField = nullptr;
}
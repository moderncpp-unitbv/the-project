#ifndef MYSTERIUM_ERRORSCREEN_H
#define MYSTERIUM_ERRORSCREEN_H


#include "BaseScreen.h"

class ErrorScreen : public BaseScreen   //Will take back the player to main menu if the lobby is full, or if it tries to choose
										//wrong role
{
public:
	ErrorScreen(sf::String error);
	
	~ErrorScreen();
	
	void updateFrame() override;
	
	void renderScreenFrame(sf::RenderWindow *window) override;

private:
	sf::Text *text;
	sf::Font *font;
	
	
};


#endif //MYSTERIUM_ERRORSCREEN_H

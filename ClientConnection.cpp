#include "ClientConnection.h"

uint16_t ClientConnection::lastID = 0;

ClientConnection::ClientConnection()
{
	this->m_id = ClientConnection::lastID++;
	this->m_player = new Player();
}

ClientConnection::ClientConnection(sf::TcpSocket *mSocket) : m_socket(mSocket)
{
	this->m_id = ClientConnection::lastID++;
	this->m_player = new Player();
}

uint16_t ClientConnection::getId() const
{
	return this->m_id;
}

void ClientConnection::setId(uint16_t id)
{
	this->m_id = id;
}

sf::TcpSocket *ClientConnection::getSocket() const
{
	return this->m_socket;
}

void ClientConnection::setSocket(sf::TcpSocket *socket)
{
	this->m_socket = socket;
}

Player *ClientConnection::getPlayer() const
{
	return this->m_player;
}

void ClientConnection::setPlayer(Player *player)
{
	this->m_player = player;
}

ClientConnection::~ClientConnection()
{
	delete m_socket;
	delete m_player;
}

uint16_t ClientConnection::getPsychicId() const
{
    return m_psychicID;
}

void ClientConnection::setPsychicId(uint16_t mPsychicId)
{
    m_psychicID = mPsychicId;
}

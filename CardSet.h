#ifndef MYSTERIUMGAME_CARDSET_H
#define MYSTERIUMGAME_CARDSET_H


#include "CharacterCard.h"
#include "LocationCard.h"
#include "ObjectCard.h"

class CardSet
{

public:

	CardSet();
	CardSet(CharacterCard*, LocationCard*, ObjectCard*);
	
	virtual ~CardSet();
	
	CharacterCard* getCharacterCard() const;
	LocationCard* getLocationCard() const;
	ObjectCard* getObjectCard() const;

private:

	CharacterCard* m_character;
	LocationCard* m_location;
	ObjectCard* m_object;
};


#endif //MYSTERIUMGAME_CARDSET_H

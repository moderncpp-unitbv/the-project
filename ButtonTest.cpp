#include <gtest/gtest.h>
#include "SFML/include/SFML/Graphics.hpp"
#include "Button.h"

class ButtonTest : public ::testing::Test
{
protected:
	void SetUp() override
	{}
	
	void TearDown() override
	{}
};

TEST_F(ButtonTest, IsNull)
{
	auto *font = new sf::Font();
	font->loadFromFile("Resources/Fonts/Magnificent.ttf");
	auto *newButton = new Button(500, 500, 200, 100, font, 40, "newButton", sf::Color::Red,
	                             sf::Color::White);
	newButton->SetIdleColor(sf::Color::Blue);
	
	EXPECT_LT(0, newButton->GetXPosition());
	
	EXPECT_LT(0, newButton->GetYPosition());
	
	EXPECT_LT(0, newButton->GetWidth());
	
	EXPECT_LT(0, newButton->GetHeight());
	
	EXPECT_LT(0, newButton->GetCharacterSize());
	
	EXPECT_FALSE(newButton->GetColor() == sf::Color::Transparent);
	
	delete font;
	delete newButton;
}
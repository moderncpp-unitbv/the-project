#pragma once
#include "Card.h"
class ObjectCard : public Card
{

public:

	static const int WIDTH = 300;
	static const int HEIGHT = 500;

public:

	ObjectCard(const uint16_t&);
	bool operator==(const ObjectCard&) const;

    uint16_t getNumber() const;

private:

	uint16_t m_number;
};


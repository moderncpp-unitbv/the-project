#ifndef MYSTERIUMGAME_INPUTFIELD_H
#define MYSTERIUMGAME_INPUTFIELD_H

#include <SFML/Graphics.hpp>
#include <functional>

class InputField
{
private:
	sf::Sprite *m_sprite;
	sf::Font *m_font;
	sf::Text *m_text;
	uint16_t m_maxLength;
	float m_width;
	float m_height;
	int m_characterSize;
	
	std::function<bool(sf::Vector2i, sf::Mouse::Button)> mousePressedFunction;

public:
	InputField(float x, float y, float width, float height, sf::Font *font, int characterSize, uint16_t maxLength);
	
	virtual ~InputField();

public:
	void UpdateText(const std::string &newText);
	
	void CenterText();
	
	void ClearText();
	
	void DeleteLastCharacter();
	
	void Select();
	
	void Deselect();
	
	bool OnMousePressed(sf::Vector2i position, sf::Mouse::Button button);
	
	std::string GetInputText();
	
	void renderInputField(sf::RenderWindow *window);

	float GetXPosition();

	float GetYPosition();

	float GetWidth();

	float GetHeight();

	unsigned int GetCharacterSize();
};


#endif //MYSTERIUMGAME_INPUTFIELD_H

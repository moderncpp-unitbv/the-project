#include <map>
#include "Logging.h"


Logging::Logging(const std::string& filename, Level minimumLevel = Level::Info, const LogOutput output=LogOutput::everywhere)
{ m_filename = filename;
	m_output = output;
	m_severity = severity; }



void Logging::Log(std::string& message, Level level)
{
	if (level >= m_minimumLevel) {
		m_output << message << std::endl;
	}
}


std::string ConvertLevelToString(const Level level)
{
	std::string result = "NONE";
	const static std::map<Level, std::string> level_strings
			{
					{ Level::Error, "ERROR"},
					{ Level::Warning, "WARNING"},
					{ Level::Info, "INFO"},
					
			};
	
	auto it = level_strings.find(level);
	if (it != level_strings.end())
	{
		result = it->second;
	}
	return result;
}

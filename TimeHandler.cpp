#include "TimeHandler.h"


TimeHandler::TimeHandler()
{
	startGameClock = sf::Clock();
	lastFrameClock = sf::Clock();
}

sf::Time TimeHandler::getTimeFromStartGame()
{
	return startGameClock.getElapsedTime();
}

sf::Time TimeHandler::getTimeFromLastFrame()
{
	return lastFrameClock.getElapsedTime();
}

sf::Time TimeHandler::resetLastFrameClock()
{
	return lastFrameClock.restart();
}

void TimeHandler::setTimer(std::function<void()> *pFunction, float seconds)
{
	timers.insert(std::pair(pFunction, seconds));
}

void TimeHandler::removeTimer(std::function<void()> *pFunction)
{
	timers.erase(pFunction);
}

void TimeHandler::updateTimers(float seconds)
{
	for (auto iterator = timers.begin(); iterator != timers.end();)
	{
		iterator->second -= seconds;
		if (iterator->second <= 0)
		{
			(*iterator->first)();
			timers.erase(iterator++);
		}
		else
		{
			++iterator;
		}
	}
}


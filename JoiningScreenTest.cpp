#include <gtest/gtest.h>
#include "SFML/include/SFML/Graphics.hpp"
#include "JoiningScreen.h"

class JoiningScreenTest : public ::testing::Test
{
protected:
	void SetUp() override
	{}
	
	void TearDown() override
	{}
};

TEST_F(JoiningScreenTest, TextExist)
{
	auto *newScreen = new JoiningScreen();
	
	EXPECT_GE(newScreen->GetTextSize(), 0);
	
	EXPECT_FALSE(newScreen->GetTextColor() == sf::Color::Transparent);
	
	EXPECT_NE(newScreen->GetTextFont(), nullptr);
	
	delete newScreen;
}
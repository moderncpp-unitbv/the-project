#include "Clock.h"

uint8_t Clock::m_hour;
sf::Sprite* Clock::m_sprite;

Clock::Clock()
{
	m_hour = 0;
	m_sprite = new sf::Sprite;
	auto texture = new sf::Texture;
	texture->loadFromFile("Resources/Textures/clock/c0.png");
	m_sprite->setTexture(*texture);
	m_sprite->setPosition(140, 30);
	m_sprite->setScale(0.5, 0.5);
}

Clock::~Clock()
{
	delete m_sprite;
}

sf::Sprite *Clock::GetSprite() const
{
	return m_sprite;
}

void Clock::RenderClock(sf::RenderWindow *window)
{
	window->draw(*m_sprite);
}

void Clock::Advance()
{
	if (m_hour < 7)
	{
		auto texture = new sf::Texture;
		texture->loadFromFile("Resources/Textures/clock/c" + std::to_string(++m_hour) + ".png");
		m_sprite->setTexture(*texture);
	}
}
#include "SFML/Graphics.hpp"

class Clock
{
public:
	
	Clock();
	~Clock();
	sf::Sprite* GetSprite() const;
	void RenderClock(sf::RenderWindow*);
	static void Advance();
	
private:
	
	static sf::Sprite* m_sprite;
	static uint8_t m_hour;
	
};
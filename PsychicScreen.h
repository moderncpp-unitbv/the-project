#include "BaseScreen.h"
#include "VisionCard.h"
#include "CharacterCard.h"
#include "Game.h"
#include "Player.h"

class PsychicScreen : public BaseScreen
{
public:

public:
	PsychicScreen();
	
	~PsychicScreen();
	
	bool IsPressed(Card *) const;
	
	void updateFrame() override;
	
	void renderScreenFrame(sf::RenderWindow *window) override;
	
	void RenderVisionCards();
	
	void MadeGuess();
	
	unsigned int GetVisionCardsSize();
	
	unsigned int GetCardSetsSize();
	
	float GetWidth();
	
	float GetHeight();

private:
	std::vector<VisionCard *> m_visionCards;
	std::vector<CardSet *> cardSets;
	LocationCard *m_selectedLocationCard;
	ObjectCard *m_selectedObjectCard;
	CharacterCard *m_selectedCharacterCard;
	std::set<CardButton *> locationButtons;
	std::set<CardButton *> characterButtons;
	std::set<CardButton *> objectButtons;
	
	Psychic *m_psychic = new Psychic();
	
	TextField *m_statusText;
	TextField *m_visionsText;
	TextField *m_pointsText;
	TextField *points;
	
};
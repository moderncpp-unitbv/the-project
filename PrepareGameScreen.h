#ifndef MYSTERIUMGAME_PREPAREGAMESCREEN_H
#define MYSTERIUMGAME_PREPAREGAMESCREEN_H

#include <iostream>
#include <set>
#include "SFML/Graphics.hpp"
#include "Game.h"
#include "BaseScreen.h"
#include "Button.h"

class PrepareGameScreen : public BaseScreen
{
public:
	PrepareGameScreen();
	
	~PrepareGameScreen();
	
	void updateFrame() override;
	
	void renderScreenFrame(sf::RenderWindow *window) override;
};


#endif //MYSTERIUMGAME_PREPAREGAMESCREEN_H

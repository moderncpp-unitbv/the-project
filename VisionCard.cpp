#include "VisionCard.h"

VisionCard::VisionCard(const uint16_t& number)
	: Card("Resources/Textures/vcards/vc" + std::to_string(number) + ".jpg"), m_number(number)
{
//	this->ResizeShape({ Width, Height });
}

bool VisionCard::operator==(const VisionCard& other) const
{
	return m_number == other.m_number;
}

uint16_t VisionCard::GetNumber() const
{
	return m_number;
}
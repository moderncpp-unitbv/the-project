#include "ObjectCard.h"

ObjectCard::ObjectCard(const uint16_t& number)
	: Card("Resources/Textures/ocards/oc" + std::to_string(number) + ".jpg"), m_number(number)
{
//	this->ResizeShape({ Width, Height });
}

bool ObjectCard::operator==(const ObjectCard& other) const
{
	return m_number == other.m_number;
}

uint16_t ObjectCard::getNumber() const
{
    return m_number;
}

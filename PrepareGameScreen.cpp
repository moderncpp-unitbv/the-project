#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma ide diagnostic ignored "modernize-use-auto"

#include "PrepareGameScreen.h"

PrepareGameScreen::PrepareGameScreen()
{
	auto width = 1920;
	auto height = 1080;
	sf::Font fontMagnificent;
	fontMagnificent.loadFromFile("Resources/Fonts/Magnificent.ttf");
	
	InputField *inputField = new InputField(width / 2.0f - 100.0f, height / 2.0f - 80.0f, 300.0f, 100.0f,
	                                        &fontMagnificent, 40, 0);
	
	inputFields.insert(inputField);
}

PrepareGameScreen::~PrepareGameScreen()
= default;

void PrepareGameScreen::updateFrame()
{
}

void PrepareGameScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);
}

#pragma clang diagnostic pop
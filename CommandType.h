#ifndef MYSTERIUMGAME_COMMANDTYPE_H
#define MYSTERIUMGAME_COMMANDTYPE_H


enum CommandType
{
	Connected = 1,
	Disconnected = 2,
	StartGame = 3,
	PlayerList = 4,
	Rejected = 5,
	RoleChange = 6,
	VisionCards = 7,
	Advance = 8,
	GhostStart = 9,
	GhostGuessInfo = 10
};


#endif //MYSTERIUMGAME_COMMANDTYPE_H

#ifndef MYSTERIUM_EXITSCREEN_H
#define MYSTERIUM_EXITSCREEN_H

#include "BaseScreen.h"

class ExitScreen: public BaseScreen
{
public:
	ExitScreen();
	
	~ExitScreen();
	
	void updateFrame() override;
	
	void renderScreenFrame(sf::RenderWindow *window) override;


private:

};


#endif //MYSTERIUM_EXITSCREEN_H

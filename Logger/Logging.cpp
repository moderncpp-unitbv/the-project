#include <map>
#include <iostream>
#include <fstream>
#include "Logging.h"
#include <chrono>
#include <ctime>


Logging::Logging(std::ostream& flux,const Level &minimumLevel , const LogOutput &output)
	:m_flux(flux),m_minimumLevel(minimumLevel),m_output(output){
	
	}
	


void Logging::Log(const std::string& message,Logging::Level level,const int &lineNumber)
{
	auto currentTime=std::chrono::system_clock::now();
	std::time_t time = std::chrono::system_clock::to_time_t(currentTime);
	
	if(!m_ofs.is_open())
	{
		m_ofs.open("loggingFile");
	}
	
	if (m_output == LogOutput::console)
	{
		if (level >= m_minimumLevel)
		{
			m_flux<<"Line: "<<lineNumber<<" , Time:";
			m_flux<<ctime(&time);
			m_flux << ConvertLevelToString(level) << ": ";
			m_flux << message << std::endl<<std::endl;
		}}
	else
	if (level >= m_minimumLevel)
	{
		m_ofs<<"Line: "<<lineNumber<<" , Time:";
		m_ofs<<ctime(&time);
		m_ofs << ConvertLevelToString(level) << ": ";
		m_ofs << message << std::endl<<std::endl;
	}
	
}


std::string Logging::ConvertLevelToString(const Logging::Level level)
{
	std::string result = "NONE";
	const static std::map<Logging::Level, std::string> level_strings
			{
					{ Logging::Level::Error, "ERROR"},
					{ Logging::Level::Warning, "WARNING"},
					{ Logging::Level::Info, "INFO"},
					
			};
	
	auto it = level_strings.find(level);
	if (it != level_strings.end())
	{
		result = it->second;
	}
	return result;
}


void Logging::ChooseOutput(Logging::LogOutput output)
{
	m_output=output;
	m_ofs.open("test");

}

void Logging::CompilationError(const int &lineNumber)
{
	std::string message="There is a compilation error";
	Log(message,Level::Error,lineNumber);
}

void Logging::GameStarted(const int &lineNumber)
{
	std::string message ="The game started";
	Log(message,Level::Info,lineNumber);
}

void Logging::PsychicJoined(const int &lineNumber)
{
	std::string message="A new Psychic joined the game";
	Log(message,Level::Info,lineNumber);
}

void Logging::PsychicLocationCorrect(const int &lineNumber)
{
	std::string message="The psychic chose the right Location Card!";
	Log(message,Level::Info,lineNumber);
}

void Logging::PsychicLocationWrong(const int &lineNumber)
{
	std::string message="The psychic chose the wrong Location Card!";
	Log(message,Level::Warning,lineNumber);
}

void Logging::PsychicCharacterCorrect(const int &lineNumber)
{
	std::string message="The psychic chose the right Character Card!";
	Log(message,Level::Info,lineNumber);
}

void Logging::PsychicCharacterWrong(const int &lineNumber)
{
	std::string message="The psychic chose the wrong Character Card!";
	Log(message,Level::Warning,lineNumber);
}

void Logging::PsychicObjectCorrect(const int &lineNumber)
{
	std::string message="The psychic chose the right Object Card!";
	Log(message,Level::Info,lineNumber);
}

void Logging::PsychicObjectWrong(const int &lineNumber)
{
	std::string message="The psychic chose the wrong Object Card!";
	Log(message,Level::Warning,lineNumber);
}


void Logging::PsychicWon(const int &lineNumber)
{
	std::string message="The psychic won the game!";
	Log(message,Level::Info,lineNumber);
}

void Logging::PsychicGuessWrong(const int &lineNumber)
{
	std::string message="The psychic guess is wrong!";
	Log(message,Level::Warning,lineNumber);
}

void Logging::PsychicGuessRight(const int &lineNumber)
{
	std::string message="The psychic guess is right!";
	Log(message,Level::Warning,lineNumber);
}

void Logging::GhostJoined(const int &lineNumber)
{
	std::string message="The ghost joined the game!";
	Log(message,Level::Info,lineNumber);
}

void Logging::GhostEndedTurn(const int &lineNumber)
{
	std::string message="The ghost ended this turn!";
	Log(message,Level::Info,lineNumber);
}

void Logging::GhostGaveCards(const int &lineNumber)
{
	std::string message="The ghost gave cards to a psychic!";
	Log(message,Level::Info,lineNumber);
}

void Logging::GhostChangedPsychic(const int &lineNumber)
{
	std::string message="The ghost changed the current psychic!!";
	Log(message,Level::Info,lineNumber);
}

void Logging::PlayerQuit(const int &lineNumber)
{
	std::string message="A player quitted the game!";
	Log(message,Level::Warning,lineNumber);
}

void Logging::GameOver(const int &lineNumber)
{
	std::string message="The game is over!";
	Log(message,Level::Info,lineNumber);
}

void Logging::GhostTaken(const int &lineNumber)
{
	std::string message="The ghost is already taken!";
	Log(message,Level::Error,lineNumber);
}

void Logging::GhostQuit(const int &lineNumber)
{
	std::string message="The ghost quitted!";
	Log(message,Level::Error,lineNumber);
}

void Logging::PsychicChooseCard(const int &lineNumber)
{
	std::string message="You should choose a card!";
	Log(message,Level::Warning,lineNumber);
}

void Logging::ServerStart(const int &lineNumber)
{
	std::string message="Server is created!";
	Log(message,Level::Info,lineNumber);
}

void Logging::ServerClosed(const int &lineNumber)
{
	std::string message="Server is closed!";
	Log(message,Level::Info,lineNumber);
}

void Logging::RoleChanged(const int &lineNumber)
{
	std::string message="A player changed his role!";
	Log(message,Level::Info,lineNumber);
}

void Logging::CardsNotGiven(const int &lineNumber)
{
	std::string message="Some psychics didn`t get the visions!";
	Log(message,Level::Warning,lineNumber);
}

void Logging::PsychicChose(const int &lineNumber)
{
	std::string message="A psychic chose a card";
	Log(message,Level::Info,lineNumber);
}

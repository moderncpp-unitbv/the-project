#ifndef MYSTERIUMGAME_LOGGING_H
#define MYSTERIUMGAME_LOGGING_H
#include <ostream>
#include <fstream>
#include <chrono>
#include <ctime>

/*#ifdef LOGGING_EXPORTS
#define LOGGING_API _declspec(dllexport)
#else
#define LOGGING_API _declspec(dllimport)
#endif*/


class /*LOGGING_API*/ Logging
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};
	
	enum class LogOutput
	{
		file,
		console
	};
	

public:
	
	Logging(std::ostream &flux=std::cout,const Level &minimumLevel = Level::Info, const LogOutput &output = LogOutput::file);
	
	std::string ConvertLevelToString(const Level level);
	
	void Log(const std::string &message, Level level,const int& lineNumber);
	
	void ChooseFile(std::string filename);
	
	void ChooseOutput(LogOutput output);
	
public:
	
	void CompilationError(const int &lineNumber);
	
	void GameStarted(const int &lineNumber);
	
	void PlayerQuit(const int &lineNumber);
	
	void GameOver(const int &lineNumber);
	
	void GhostTaken (const int &lineNumber);
	
	void ServerStart(const int &lineNumber);
	
	void ServerClosed(const int &lineNumber);
	
	void RoleChanged(const int&lineNumber);
	
public:
	void PsychicJoined(const int &lineNumber);
	
	void PsychicLocationCorrect(const int &lineNumber);
	
	void PsychicLocationWrong(const int &lineNumber);
	
	void PsychicCharacterCorrect(const int &lineNumber);
	
	void PsychicCharacterWrong(const int &lineNumber);
	
	void PsychicObjectCorrect(const int &lineNumber);
	
	void PsychicObjectWrong(const int &lineNumber);
	
	void PsychicWon(const int &lineNumber);
	
	void PsychicGuessWrong(const int &lineNumber);
	
	void PsychicGuessRight(const int &lineNumber);
	
	void PsychicChooseCard(const int &lineNumber);
	
	void PsychicChose(const int&lineNumber);
	
public:
	void GhostJoined(const int &lineNumber);
	
	void GhostEndedTurn(const int &lineNumber);
	
	void GhostGaveCards(const int &lineNumber);
	
	void GhostChangedPsychic(const int &lineNumber);
	
	void GhostQuit (const int &lineNumber);
	
	void CardsNotGiven(const int&lineNumber);
	

private:
	Level m_minimumLevel;
	LogOutput m_output;
	std::string m_filename;
	std::ostream &m_flux;
	std::ofstream m_ofs;
	
};

#endif //MYSTERIUMGAME_LOGGING_H

#include <SFML/Audio.hpp>
#include <vector>

class SoundHandler
{
public:
	SoundHandler();
	void toggleMusic();
	void playClickSound();

public:
	inline static SoundHandler *instance;
	
private:
	// https://www.youtube.com/watch?v=yuPg_W_LTcY&ab_channel=Marcv%2FdMeulen
	std::unique_ptr<sf::Music> theme;
	
	std::pair<sf::SoundBuffer, sf::Sound> click;
	
	bool isPlaying;
};

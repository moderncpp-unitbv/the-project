#include "CharacterCard.h"

CharacterCard::CharacterCard(const uint16_t& number)
	: Card("Resources/Textures/ccards/cc" + std::to_string(number) + ".jpg"), m_number(number)
{
}

bool CharacterCard::operator==(const CharacterCard& other) const
{
	return m_number == other.m_number;
}

uint16_t CharacterCard::getNumber() const
{
    return m_number;
}


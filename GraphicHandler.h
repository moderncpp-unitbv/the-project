#ifndef MYSTERIUMGAME_GRAPHICHANDLER_H
#define MYSTERIUMGAME_GRAPHICHANDLER_H

#include <SFML/Graphics.hpp>
#include "BaseScreen.h"
#include "MainMenuScreen.h"
#include "Game.h"

class GraphicHandler
{
public:
	
	std::map<std::string, sf::Texture *> textures = {
			{"background", new sf::Texture()},
			{"logo",       new sf::Texture()},
			{"logoPL",     new sf::Texture()}
	};
	
	std::map<std::string, sf::Sprite *> sprites = {
			{"background", new sf::Sprite()},
			{"logo",       new sf::Sprite()},
			{"logoPL",     new sf::Sprite()}
	};
	
	sf::RenderWindow *window;
	BaseScreen *actualScreen;
	BaseScreen *targetScreen;
	
	virtual ~GraphicHandler();
	
	void init();
	
	void initWindow();
	
	void setTargetScreen(BaseScreen *screen);
	
	void changeScreen();
	
	void SetTexture(const std::string &, const std::string &);
	
	void renderGraphicFrame() const;
};


#endif //MYSTERIUMGAME_GRAPHICHANDLER_H

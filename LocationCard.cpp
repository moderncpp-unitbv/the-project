#include "LocationCard.h"

LocationCard::LocationCard(const uint16_t& number)
	: Card("Resources/Textures/lcards/lc" + std::to_string(number) + ".jpg"), m_number(number)
{
	
}

bool LocationCard::operator==(const LocationCard& other) const
{
	return m_number == other.m_number;
}

uint16_t LocationCard::getNumber() const
{
    return m_number;
}

#include "JoiningScreen.h"
#include "LobbyScreen.h"
#include "Game.h"
#include "ErrorScreen.h"

TextField *JoiningScreen::rejectedMessage = nullptr;
bool JoiningScreen::showRejectedMessage = false;

JoiningScreen::JoiningScreen()
{
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;

	auto widthButton = width / 6.6f;
	auto heightButton = height / 10.8f;

	auto characterSize = height / 27;

	font = new sf::Font();
	font->loadFromFile("Resources/Fonts/Magnificent.ttf");

	textField = new TextField(width / 2.0f, height / 2.0f - heightButton * 1.4, width / 2, height / 2, font,
	                          characterSize, "", sf::Color(120, 120, 120));
	textField->SetText("Enter the target IP: ", width, height / 1.3f);


	auto *BackButton = new Button(width - widthButton - 20.f, height - heightButton - 20.f, widthButton, heightButton,
	                              font, characterSize, "BACK",
	                              sf::Color(150, 150, 150), sf::Color::White);

	std::function<void()> BackButtonFunction = []()
	{
		Game::instance->getGraphicHandler()->setTargetScreen(new MainMenuScreen());
	};

	BackButton->addActionOnPress((BackButtonFunction));
	buttons.insert(BackButton);

	auto *inputField = new InputField(width / 2.0f - widthButton / 2, height / 2.f - heightButton, widthButton,
	                                  heightButton - 30, font, characterSize, 15);

	inputFields.insert(inputField);

	auto *EnterCodeButton = new Button(width / 2.0f - widthButton / 2, height / 2.f, widthButton, heightButton,
	                                   font, characterSize, "ENTER",
	                                   sf::Color(150, 150, 150), sf::Color::White);

	std::function<void()> EnterCodeButtonFunction = [inputField]()
	{
		std::string inputText = inputField->GetInputText();
		if (Server::isValidIPAddress(inputText))
		{
			sf::IpAddress ipAddress(inputText);
			Game::instance->GetClient()->StartConnectThread(ipAddress);
		} else
		{
			inputField->ClearText();
			std::cout << "Not a valid IP Address." << std::endl;
		}
	};

	EnterCodeButton->addActionOnPress(EnterCodeButtonFunction);
	buttons.insert(EnterCodeButton);

	rejectedMessage = new TextField(width / 2.0f, height / 1.7f, width / 2, height / 2, font,
	                                characterSize, "You have been rejected by the server!", sf::Color::Red);

	showRejectedMessage = false;
}

JoiningScreen::~JoiningScreen()
= default;

void JoiningScreen::updateFrame()
{
}

void JoiningScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);

	textField->renderTextField(window);

	if(showRejectedMessage)
	{
		rejectedMessage->renderTextField(window);
	}
}

unsigned int JoiningScreen::GetTextSize()
{
	return this->textField->GetTextSize();
}

sf::Color JoiningScreen::GetTextColor()
{
	return this->textField->GetTextColor();
}

const sf::Font *JoiningScreen::GetTextFont()
{
	return this->font;
}

TextField *JoiningScreen::getRejectedMessage()
{
	return rejectedMessage;
}

bool JoiningScreen::isShowRejectedMessage()
{
	return showRejectedMessage;
}

void JoiningScreen::setShowRejectedMessage(bool showRejectedMessage)
{
	JoiningScreen::showRejectedMessage = showRejectedMessage;
}

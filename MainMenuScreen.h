#ifndef MYSTERIUM_MAINMENU_H
#define MYSTERIUM_MAINMENU_H

#include "SFML/Graphics.hpp"
#include "GraphicHandler.h"
#include "Button.h"

class MainMenuScreen : public BaseScreen
{

public:
	MainMenuScreen();
	
	~MainMenuScreen();
	
	void updateFrame() override;
	
	void renderScreenFrame(sf::RenderWindow *window) override;
};


#endif //MYSTERIUM_MAINMENU_H

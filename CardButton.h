#include <functional>
#include <thread>
#include "SFML/Graphics.hpp"
#include "Card.h"
#include "VisionCard.h"

class CardButton
{
private:
	sf::RectangleShape *shape;
	Card *m_card;
	
	sf::Color idleColor;
	sf::Color hoverColor;
	sf::Color activeColor;

	bool hovering;
	bool pressed;
	bool m_visible;
	
	std::vector<std::function<void()>> actionsOnPress;
	
	std::function<bool(sf::Vector2i)> mouseMovedFunction;
	std::function<bool(sf::Vector2i, sf::Mouse::Button)> mousePressedFunction;
	std::function<void()> colorBackFunction;

public:
	CardButton(float, float, float, float, Card*);
	CardButton(float, float, Card*);
	~CardButton();

public:
	void renderButton(sf::RenderWindow *window);
	
	bool onMouseHover(sf::Vector2i);
	
	bool onMousePressed(sf::Vector2i, sf::Mouse::Button);
	
	void pressButton();
	
	void addActionOnPress(const std::function<void()> &pFunction);
	
	void removeActionOnPressIndex(int index);
	
	void Reset();
	
	bool IsPressed() const;
	
	bool IsVisible() const;
	
	void SetVisible(bool);
	
	Card *GetCard() const;
	
	void SetCard(Card*);
};
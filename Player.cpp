#include "Player.h"
#include <utility>

Player::Player()
		: m_name("INVALID NAME"), m_role(PlayerRole::None_Role), m_choosingType(ChoosingType::CHARACTER)
{
	m_ghost = nullptr;
	m_psychic = nullptr;
}

Player::Player(std::string name)
		: m_name(std::move(name)), m_role(PlayerRole::None_Role), m_choosingType(ChoosingType::CHARACTER)
{
	m_ghost = nullptr;
	m_psychic = nullptr;
}

void Player::SetRole(PlayerRole targetRole)
{
	this->m_role = targetRole;
}

void Player::SetName(std::string targetName)
{
	this->m_name = std::move(targetName);
}

std::string Player::GetName() const
{
	return this->m_name;
}

PlayerRole Player::GetRole() const
{
	return this->m_role;
}

std::string Player::GetRoleName() const
{
	switch (this->m_role)
	{
		case PlayerRole::None_Role:
		{
			return "None";
		}
		case PlayerRole::Ghost_Role:
		{
			return "Ghost";
		}
		case PlayerRole::Psychic_Role:
		{
			return "Psychic";
		}
		default:
		{
			return "ERROR";
		}
	}
}

sf::Packet &operator<<(sf::Packet &packet, const Player &player)
{
	return packet << player.GetName() << (int8_t) player.GetRole();
}

sf::Packet &operator>>(sf::Packet &packet, Player &player)
{
	std::string playerName;
	packet >> playerName;
	player.SetName(playerName);
	
	sf::Int8 playerRoleInt;
	packet >> playerRoleInt;
	player.SetRole((PlayerRole) playerRoleInt);
	
	return packet;
}

ChoosingType Player::GetChoosingType() const
{
	return m_choosingType;
}

std::string Player::GetChoosingTypeName() const
{
	switch (m_choosingType)
	{
		case ChoosingType::CHARACTER:
			return "CHARACTER";
		case ChoosingType::LOCATION:
			return "LOCATION";
		case ChoosingType::OBJECT:
			return "OBJECT";
		case ChoosingType::FINAL:
			return "FINAL";
	}
}

void Player::AdvanceChoosing()
{
	switch (m_choosingType)
	{
		case ChoosingType::CHARACTER:
			std::cout << "You chose the correct character card!";
			m_choosingType = ChoosingType::LOCATION;
			m_psychic->ResetVisions();
			break;
		case ChoosingType::LOCATION:
			std::cout << "You chose the correct location card!";
			m_choosingType = ChoosingType::OBJECT;
            m_psychic->ResetVisions();
			break;
		case ChoosingType::OBJECT:
			std::cout << "You chose the correct object card!";
			m_choosingType = ChoosingType::FINAL;
            m_psychic->ResetVisions();
			break;
		case ChoosingType::FINAL:
			break;
	}
}

Ghost *Player::getGhost() const
{
	return m_ghost;
}

void Player::setGhost(Ghost *mGhost)
{
	m_ghost = mGhost;
}

Psychic *Player::getPsychic() const
{
	return m_psychic;
}

void Player::setPsychic(Psychic *mPsychic)
{
	m_psychic = mPsychic;
}

Ghost *Player::InitializeGhost()
{
	m_ghost = new Ghost();
}

Psychic *Player::InitializePsychic(CardSet *clue, uint8_t psychicIndex)
{
	m_psychic = new Psychic(clue, psychicIndex);
}

Player::~Player()
{
	if (m_ghost != nullptr)
	{
		delete m_ghost;
	}
	
	if (m_psychic != nullptr)
	{
		delete m_psychic;
	}
}

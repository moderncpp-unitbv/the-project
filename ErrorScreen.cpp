#include "ErrorScreen.h"
#include "MainMenuScreen.h"

ErrorScreen::ErrorScreen(sf::String error)
{
	auto width = sf::VideoMode::getDesktopMode().width;
	auto height = sf::VideoMode::getDesktopMode().height;
	
	auto widthButton = width / 6.6f;
	auto heightButton = height / 10.8f;
	
	auto characterSize = height / 27;
	
	font = new sf::Font();
	font->loadFromFile("Resources/Fonts/Magnificent.ttf");
	
	text = new sf::Text();
	text->setCharacterSize(characterSize);
	text->setFillColor(sf::Color::Black);
	text->setOutlineColor(sf::Color(165, 214, 244));
	text->setOutlineThickness(2);
	text->setFont(*font);
	text->setString(error);
	
	//center text
	sf::FloatRect textRect = text->getLocalBounds();
	text->setOrigin(textRect.left + textRect.width / 2.0f,
	                textRect.top + textRect.height / 2.0f);
	text->setPosition(sf::Vector2f(width / 2.0f, height / 2.0f));
	
	auto *BackButton = new Button(width - widthButton - 20.f, height - heightButton - 20.f, widthButton, heightButton,
	                              font, characterSize, "BACK",
	                              sf::Color(150, 150, 150), sf::Color::White);
	
	std::function<void()> BackButtonFunction = []()
	{
		Game::instance->getGraphicHandler()->setTargetScreen(new MainMenuScreen());
	};
	
	BackButton->addActionOnPress((BackButtonFunction));
	buttons.insert(BackButton);
	

}

ErrorScreen::~ErrorScreen()
= default;

void ErrorScreen::updateFrame()
{
}

void ErrorScreen::renderScreenFrame(sf::RenderWindow *window)
{
	BaseScreen::renderScreenFrame(window);
	window->draw(*text);
}

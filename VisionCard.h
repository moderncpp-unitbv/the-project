#pragma once
#include "Card.h"
class VisionCard : public Card
{

public:

	static const int WIDTH = 300;
	static const int HEIGHT = 500;

public:

	VisionCard(const uint16_t&);
	bool operator==(const VisionCard&) const;
	uint16_t GetNumber() const;

private:

	uint16_t m_number;

};


#include <cstdint>
#include <string>

enum class ChoosingType : uint8_t
{
	CHARACTER,
	LOCATION,
	OBJECT,
	FINAL
};


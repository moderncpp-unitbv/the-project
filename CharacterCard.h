#pragma once
#include "Card.h"
class CharacterCard : public Card
{

public:

	CharacterCard(const uint16_t&);
	bool operator==(const CharacterCard&) const;


    uint16_t getNumber() const;

private:

	uint16_t m_number;
};

